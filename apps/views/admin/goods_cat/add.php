<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label"> 分类名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[names]" class="layui-input" lay-verify="required" >
				</div>
			</div>
			<div class="layui-form-item">
					<label class="layui-form-label">分类图片</label>
					<div class="layui-input-block">
						<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片尺寸100*100px</span>
						<div class="layui-upload layui-hide">
						  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
						  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
					  	</div>
					  	<input name="data[thumb]" class="thumb" type="hidden" required />
					</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
	$('.f_file').f_upload();
</script>
<?php echo template('admin/footer');?>