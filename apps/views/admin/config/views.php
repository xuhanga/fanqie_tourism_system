<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">配置</div>
		</blockquote>
		<form class="layui-form layui-form-pane a-e-form" method="post">
		<?php foreach($items as $v) {if($v['imports'] == 'text') {?>
			<div class="layui-form-item">
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-inline">
					<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
					<input type="text" name="data[<?php echo $v['id'];?>][val]" value="<?php echo $v['val'];?>" class="layui-input" placeholder="<?php echo $v['cname']?>" lay-verify="required">
				</div>
				<div class="layui-form-mid layui-word-aux"><?php echo $v['bak']?></div>
			</div>
        <?php }elseif($v['imports'] == 'radio'){?>
            <div class="layui-form-item" pane>
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-block">
					<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
					<input type="checkbox" name="data[<?php echo $v['id'];?>][val]" value="1" lay-skin="switch" lay-text="开启|关闭" <?php if($v['val']==1){ echo 'checked';}?>> <i></i>
				</div>
			</div>
		<?php }elseif($v['imports'] == 'image'){?>
			<input type="hidden" name="data[hids][<?php echo $v['id'];?>]" value="<?php echo $v['id'];?>"> 
			<div class="layui-form-item">
				<label class="layui-form-label"><?php echo $v['cname']?></label>
				<div class="layui-input-block">
						<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say"><?php echo $v['bak']?></span>
						<div class="layui-upload layui-hide">
						  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
						  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
					  	</div>
					  	<input name="data[<?php echo $v['id'];?>][val]" class="thumb" type="hidden" value="<?php echo $v['val'];?>" />
				</div>
			</div>
		<?php }}?>
            <div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('adminct/config/quicksave'),'save','layui-btn-lg',"lay-filter='sub' location=''")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
});
</script>
<?php echo template('admin/footer');?>
