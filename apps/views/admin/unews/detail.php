<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">订单详情</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col></colgroup>
					<tbody>
						<tr>
							<td style="text-align: center;"><?php echo $item['content'];?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>