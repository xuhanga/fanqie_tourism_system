<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form" id="order" method="get">
						<div class="layui-input-inline">
							<select name="tid" id="tid">
								<option value="0">选择活动</option>
								<?php foreach ($tourism as $v){?>
								<option value="<?php echo $v['id'];?>"><?php echo $v['title'];?></option>
								<?php }?>
				        	</select>
				        </div>
				        <div class="layui-input-inline">
							<select name="state" id="state">
								<option value="0">订单状态</option>
								<?php foreach ($state as $k=>$v){?>
								<option value="<?php echo $k;?>"><?php echo $v;?></option>
								<?php }?>
				        	</select>
				        </div>
				        <div class="layui-input-inline">
							<input type="text" placeholder="出发时间" id="stime" name="stime" class="layui-input time" >
						</div>
						<div class="layui-input-inline">
							<input type="text" placeholder="结束时间" id="etime" name="etime" class="layui-input time" >
						</div>
						<div class="layui-input-inline">
							<input type="text"  id="srk" name="srk" placeholder="请输入订单号" class="layui-input" >
						</div>
						<div class="layui-input-inline">
					    <?php echo admin_btn('', 'find',"",'lay-filter="order-find"')?>
					    </div>
					</form>
				</div>
				<div class="layui-inline">
				<?php echo admin_btn(site_url("$dr_url/export?"),'btn','layui-btn-normal',"id='exp'",'导出');?>
				</div>
				<div class="layui-inline">
					<form method="get">
					<?php echo admin_btn(site_url("$dr_url/balance"),'btn','layui-btn-warm',"lay-submit lay-filter='sub' location='#' ",'一键核销')?>
					</form>
				</div>
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
	<?php echo admin_btn(site_url($dr_url.'/detail/id-{{d.id}}'),'','layui-btn-xs layui-btn-normal','','详');?>
	<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="zt">
{{# if(d.state==1){ }}
<span class='layui-btn layui-btn-xs layui-bg-red'>待支付</span>
{{# }else if(d.state==2){ }}
<span class='layui-btn layui-btn-xs layui-bg-orange'>已报名</span>
{{# }else if(d.state==3){ }}
<span class='layui-btn layui-btn-xs layui-bg-green'>已结束</span>
{{# }else if(d.state==4){ }}
<span class='layui-btn layui-btn-xs layui-bg-cyan'>已退出</span>
{{# }else if(d.state==5){ }}
<span class='layui-btn layui-btn-xs layui-bg-blue'>申请退出</span>
{{# } }}
</script>
<script>
$(function(){
	$('#exp').click(function(){
		var url = $(this).attr('url');
		location.href = url + $('#order').serialize();
		
	});
	$('.time').each(function(){
		layui.laydate.render({ 
			  elem: this
		});	
	});
});
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo isset($ttid)?$dr_url."/lists/ttid-$ttid-tid-$tid":$dr_url.'/lists'?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'oid', title: '订单号'},
	       {field: 'title', title: '订单名'},
	       {field: 'stime', title: '出发时间',toolbar:'<div>{{# if(d.etime){ }} {{d.stime}} ~ {{d.etime}} {{# }else{ }} {{d.stime}} {{# } }}</div>'},
	       {field: 'total', title: '价格'},
	       {field: 'xl', title: '线路'},
	       {field: 'state', title: '状态',toolbar:"#zt", width: 120},
	       {field: 'addtime', title: '下单时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});

//数据数据 异步提交查询
layui.form.on('submit(order-find)',function(){
	layui.table.reload('common',{//这里的find 是为了后台数据处理
		where:{tid:$('#tid').val(),state:$('#state').val(),stime:$('#stime').val(),etime:$('#etime').val(),srk:$('#srk').val(),find:'find',total:''},
		done:function(res, curr, count){
			this.where.total = count;
			this.where.find = '';
		}
	});
	return false;
});
</script>