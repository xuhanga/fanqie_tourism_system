<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">基本资料</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col><col width="150"><col></colgroup>
					<tbody>
						<tr>
							<td>姓名</td><td ><?php echo $item['username'];?></td>
							<td>电话</td><td ><?php echo $item['mobile'];?></td>
						</tr>
						<tr>
							<td>简介</td><td colspan="3"><?php echo $item['content'];?></td>
						</tr>
					</tbody>
				</table>
				<blockquote class="layui-elem-quote title">照片</blockquote>
				<table class="layui-table">
					<colgroup><col width="150"><col></colgroup>
					<?php $thumb = explode(',', $item['thumb']);?>
					<tbody>
						<?php foreach ($thumb as $k=>$v){?>
						<tr>
							<td>照片</td><td class="img_view"><img src="<?php echo $v;?>"></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript">
layer.photos({photos:'.img_view'});//添加预览
</script>
<?php echo template('admin/footer');?>