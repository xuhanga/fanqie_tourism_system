<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title><?php echo SYSTEM_NAME;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="chaituan@126.com">
<link rel="stylesheet" href="<?php echo LAYUI."css/layui.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."admin/main.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."font-awesome.min.css";?>" type="text/css" />
</head>
<body >
<div class="layui-layout">
<div>
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
					<div class="layui-input-inline">
						<input type="text"  id="table-find-val" placeholder="请输入昵称" class="layui-input" lay-verify='required'>
					</div>
				    <?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
		</blockquote>
		<table  id="user" lay-filter="leader" ></table>
	</div>
</div>
<div id="ckd" style="display: none;"></div>
<?php echo template('admin/script');?>
<script>
//执行渲染
var tab = layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full', //容器高度
	url:'<?php echo site_url("$dr_url/l_lists_iframe")?>',
	cols: [[
	       {field: 'cks',checkbox: true},
	       {field: 'id', title: 'ID', width: 80,sort:true},
	       {field: 'nickname', title: '昵称'},
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		$(".layui-table-header input[name='layTableCheckbox']").attr("disabled",true);
		$(".layui-table-header .layui-form-checkbox").css("display",'none');
	}
});
layui.table.on('checkbox(leader)', function(obj){
	if(obj.type!='all'){
		if(obj.checked){
			$('#ckd').append('<input type="hidden" name="xz" id="ckd_list_'+obj.data.id+'" value="'+obj.data.id+'" data-name="'+obj.data.nickname+'" >');
		}else{
			$('#ckd_list_'+obj.data.id).remove();
		}
	}
});
</script>
</div>
</body>
</html>