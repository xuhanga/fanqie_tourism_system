<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">添加领队</label>
					<div class="layui-input-block">
						<span class="layui-btn layui-btn-primary" id="add_leader">添加(超过4个可能会出问题)</span>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">出发时间</label>
						<div class="layui-input-inline" >
							<input type="text" name="data[stime]" class="layui-input time" lay-verify="required" >
						</div>
						<?php if($item['day_num']<=1){?> 
						<div class="layui-form-mid layui-word-aux">当前主活动只有一天，只选择出发时间即可</div>
						<?php }?>
					</div>
					<?php if($item['day_num']>=2){?>
					<div class="layui-inline">
						<label class="layui-form-label">结束时间</label>
						<div class="layui-input-inline" >
							<input type="text" name="data[etime]" class="layui-input time"  >
						</div>
					</div>
					<?php }?>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">成行人数</label>
						<div class="layui-input-inline" >
							<input type="text" name="data[in_line]" class="layui-input"  value="<?php echo $item['in_line'];?>" lay-verify="required|number" >
						</div>
					</div>
					<div class="layui-inline">
						<label class="layui-form-label">满员人数</label>
						<div class="layui-input-inline" >
							<input type="text" name="data[full]" class="layui-input"  value="<?php echo $item['full'];?>"  lay-verify="required|number" >
						</div>
					</div>
				</div>
				<div class="layui-form-item">
						<label class="layui-form-label">群二维码</label>
						<div class="layui-input-block">
							<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
							<div class="layui-upload layui-hide">
							  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
							  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
						  	</div>
						  	<input name="data[qun]" class="thumb" type="hidden" lay-verify='thumb' />
						</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
					<input type="hidden" name="data[line_num]" value="<?php echo $item['line_num'];?>">
					<input type="hidden" name="data[tid]" value="<?php echo $item['id'];?>">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
					</div>
				</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$('.f_file').f_upload();
$(function(){
	$('#add_leader').click(function(){
		var that = this;
		layer.open({
			type:2,
			content:'/adminct/user/leader_iframe',
			area: ['60%', '60%'],
			btn:'选择插入',
			btnAlign: 'c',
			yes:function(index){
				var body = layer.getChildFrame('body', index);
				$(body.find(".layui-layout #ckd input[name='xz']")).each(function(k) {
					var name = $(this).data('name');
                    var id = $(this).val();
                    var h = '<input type="checkbox" name="data[leader]['+id+']" title="'+name+'" value="'+id+'" checked>'
                    $(that).before(h);
                    layui.form.render('checkbox');
                });
				layer.closeAll('iframe');
			}
		});
	});
	$('.time').each(function(){
		layui.laydate.render({ 
			  elem: this
		});	
	});
});
</script>
<?php echo template('admin/footer');?>