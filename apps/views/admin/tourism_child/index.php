<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
						<div class="layui-input-inline">
							<input type="text"  id="table-find-val" placeholder="请输入活动名称" class="layui-input" lay-verify='required'>
						</div>
				    	<?php echo admin_btn('', 'find',"",'lay-filter="table-find"')?>
					</form>
				</div>
		</blockquote>
		<table  id="user" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">

<?php echo admin_btn(site_url($dr_url.'/edit/id-{{d.id}}'),'edit','layui-btn-xs');?>
<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="sponsor">
{{# if(d.uid==0){ }}官方{{# }else{ }}{{d.nickname}}{{# } }}
</script>
<script type="text/html" id="time">
{{# if(d.etime){ }} {{d.stime}} ~ {{d.etime}}  {{# }else{ }} {{d.stime}}  {{# } }}
</script>
<script type="text/html" id="sign">
{{# if(parseInt(d.sign_num) < parseInt(d.in_line)){ }}
	<span class="layui-badge layui-bg-blue">报名中</span>
{{# }else if(parseInt(d.sign_num) >= parseInt(d.in_line)&&parseInt(d.sign_num) < parseInt(d.full)){ }}
	<span class="layui-badge layui-bg-orange">已成行</span>
{{# }else if(parseInt(d.sign_num) >= parseInt(d.full)){ }}
	<span class="layui-badge">已满员</span>
{{# } }}
</script>
<script type="text/html" id="state">
{{# if(parseInt(d.state) == 1){ }}
	<span class="layui-badge layui-bg-green">上线中</span>
{{# }else{ }}
	{{# if(parseInt(d.qun)){ }}
		<span class="layui-badge">待上线</span>
	{{# }else{ }}
		<span class="layui-badge">二维码未分配</span>
	{{# } }}
{{# } }}
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#user', //指定原始表格元素选择器（推荐id选择器）
	id:'common',//给事件用的
	height: 'full-250', //容器高度
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'sort', title: '排序', width: 80,edit:'text'},
	       {field: 'uid', title: '活动发起人',toolbar:'#sponsor'},
	       {field: 'title', title: '发起活动名'},
	       {field: 'stime', title: '活动时间',toolbar:'#time'},
	       {field: 'sign_num', title: '已报名人数', width: 120},
	       {field: 'sign', title: '报名状态',templet:'#sign'},
	       {field: 'state', title: '上线状态',toolbar:'#state'},
	       {field: 'addtime', title: '发起时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'right', title: '操作',toolbar:'#operation', width: 220}
	       ]],
	limit: 15,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
	}
});
layui.table.on('edit(common)', function(obj){
    var value = obj.value ,data = obj.data;
    $.post('<?php echo site_url("$dr_url/sort")?>',{id:data.id,sort:value},function(d){layer.msg(d.message)},'json');
  });
</script>
<?php echo template('admin/footer');?>