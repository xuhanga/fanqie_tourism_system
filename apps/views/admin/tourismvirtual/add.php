<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">

		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">姓名</label>
					<div class="layui-input-block">
						<input type="text" name="data[nickname]" class="layui-input time"  lay-verify='required'>
					</div>
				</div>
				<div class="layui-form-item">
						<label class="layui-form-label">头像</label>
						<div class="layui-input-block">
							<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px</span>
							<div class="layui-upload layui-hide">
							  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
							  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
						  	</div>
						  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify='thumb' />
						</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">人数</label>
					<div class="layui-input-block">
						<input type="text" name="data[num]" class="layui-input" value="1" lay-verify='required'>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<input type="hidden" name="data[tcid]" value="<?php echo $tcid;?>" lay-verify='required'>
						<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url/id-$tcid'")?>
					</div>
				</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$('.f_file').f_upload();
</script>
<?php echo template('admin/footer');?>