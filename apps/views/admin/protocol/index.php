<?php $data['definedcss'] = array(PLUGIN.'umeditor/themes/default/css/umeditor.min');echo template('admin/header',$data);echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">旅游协议</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
				<form class="layui-form  a-e-form" method="post">
						<div class="layui-form-item">
								<script type="text/plain" id="emditor" name="content"><?php echo isset($item['content'])?$item['content']:''; ?></script>
						</div>
						<div class="layui-form-item">
							<div class="layui-input-block">
								<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location=''")?>
							</div>
						</div>
			</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script src="<?php echo PLUGIN.'umeditor/third-party/template.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.config.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/umeditor.min.js'?>"></script>
<script src="<?php echo PLUGIN.'umeditor/lang/zh-cn/zh-cn.js'?>"></script>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
	var um = UM.getEditor('emditor');
	um.setWidth("100%");
	$(".edui-body-container").css("width", "98%");
});
</script>
<?php echo template('admin/footer');?>