<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/swipebox.min',CSS_PATH.'mobile/index'); echo template('mobile/header',$data);?>
<div class="top flex-csb bg-ff plr12 ptb8 mb15">
		<a href="javascript:history.back()">
			<svg class="icon c-diy" aria-hidden="true" ><use xlink:href="#icon-zuojiantou"></use></svg>
		</a>
		<span class="f15 fw-bold c-diy">队友足迹</span>
		<a href="/">
			<svg class="icon c-diy" aria-hidden="true" font-size="0.25rem"><use xlink:href="#icon-shouye-copy"></use></svg>
		</a>
</div>
<ul class="travel-list bg-ff mtb15 mb60">
<?php foreach ($items as $v){?>
	<li class="plr12 ptb15 bb-e5">
		<div class="travel-info flex-cs">
			<img src="<?php echo $v['items']['u_thumb'];?>"  class="br-circle">
			<span class="f14 pl8"><?php echo $v['items']['nickname'];?></span>
		</div>
		<div class="pt8 f14"><?php echo $v['items']['content'];?></div>
		<div class="upload-pane flex-cs r-wrap" data-id="111">
		<?php if(array_filter($v['thumb'])){ foreach ($v['thumb'] as $t){?>
			<div class="w3 mtb8 upload-list p-r swipebox flex-center" href="<?php echo $t;?>" style="height:0.92rem;overflow:hidden">
				<img src="<?php echo $t;?>" class="travel-img" alt="image" style="width:95%">
			</div>
		<?php }}?>
		</div>
		<p class="f13 c9 flex-csb">
		<span><?php echo format_time($v['items']['addtime'])?></span>  
		<span>
			<span class="star" data-id="<?php echo $v['items']['id'];?>">
			<svg class="icon" aria-hidden="true"  font-size="0.16rem"><use xlink:href="#icon-dianzan"></use></svg>
			<span class="star-num"><?php echo $v['items']['star'];?></span>
			</span>
			<span class="comment ml10 ct_comment_load" data-id="<?php echo $v['items']['id']?>"><svg class="icon" aria-hidden="true"  font-size="0.16rem"><use xlink:href="#icon-pinglun"></use></svg></span>
		</span>
		</p>
		<div id="comment_lists_<?php echo $v['items']['id']?>" class="flex-dc" <?php echo isset($cmt[$v['items']['id']])?"":"style='display: none;'";?>>
			<em class="top-triangle bg-ee-bc ml10"></em>
			
			<div class="f13 c9 bg-ee plr10 ptb10 " id="comment_list_<?php echo $v['items']['id']?>" >
			<?php if(isset($cmt[$v['items']['id']])) foreach ($cmt[$v['items']['id']] as $cv){?>
			<p><?php echo $cv['nickname'].': '.$cv['says'];?></p>
			<?php } ?>
			</div>
		</div>
	</li>
	<?php }?>
</ul>
<a href="<?php echo site_url('mobile/travel/upload')?>" class="upload-btn p8 br-circle bg-diy">
	<svg class="icon" aria-hidden="true" color="#fff" font-size="0.25rem">
		<use xlink:href="#icon-bianji"></use>
	</svg>
</a>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/jquery.swipebox.min.js'?>" ></script>

<script>
$(function(){
//   $('.ct_comment_load').click(function(){
// 		var id = $(this).data('id');
// 		$.get("/mobile/travel/get_cmt/id-"+id,function(d){
// 	 		if(d.state==1){
// 	 			$('#comment_lists_'+id).show();
// 	 			var ds = d.data;
// 	 			for(i=0;i<ds.length;i++){
// 	 				$('#comment_list_'+id).append('<p>'+ds[i].nickname+': '+ds[i].says+'</p>');
// 		 		}
// 		 	}
// 		},'json');
// 	});
// 	$('.ct_comment_load').trigger("click");
	$(".comment").click(function(){
		var id = $(this).data('id');
		var name = '<?php echo $U['nickname']?>';
		layer.open({
			content: '<textarea rows="5" class="p10" style="width:90%;border:1px solid #eee;border-radius:5px;" placeholder="请输入评论" id="text"></textarea>',
			btn:'提交',
			yes:function(index){
				var v = $('#text').val()
				if(v){
			 		$.post("/mobile/travel/cmt",{id:id,say:v},function(d){
				 		if(d.state==1){
				 			$('#comment_lists_'+id).show();
				 			$('#comment_list_'+id).append('<p>'+name+': '+v+'</p>');
					 	}
			 			layer_msg(d.message)
		 			},'json');
				}else{
					layer.msg('不能为空')
				}
			},
			success:function(){
				$('.layui-m-layercont').css('padding','30px 5% 20px 5%');
			}
		});
	});
	  
  	$('.upload-list').each(function(){
  		var boxH = $(this).outerHeight();
  		var imgH = $(this).children('.travel-img').outerHeight();
  		if(imgH<boxH){
	  		$(this).children('.travel-img').addClass('as-st');
	  	}

  	})
	  $( '.swipebox' ).swipebox();
	  $('.star').click(function(){
			var id = $(this).data('id');
			var that = this;
			$.get('/mobile/travel/star/id-'+id,function(d){
				if(d.state==1){
					$(that).css('color','red');
					var star = parseInt($(that).children('.star-num').text());
					star = star + 1;
					$(that).children('.star-num').text(star);
				}
				layer_msg(d.message);
			},'json');
			
	  });
  })
</script>
</body>
</html>