 	<!--地址列表-->
<div id="_userList" style="display:none">
	<div class="top flex-csb bg-ff plr12 ptb8">
		<a href="javascript:void(0)" onclick="javascript:location.hash='paypage'">
		<svg class="icon" aria-hidden="true" color="#3ec77d">
		    <use xlink:href="#icon-zuojiantou"></use>
		</svg>
		</a>
		<span class="f15 fw-bold">选择常用人</span>
		<a href="/">
		<svg class="icon" aria-hidden="true" font-size="0.25rem" color="#3ec77d">
		    <use xlink:href="#icon-shouye-copy"></use>
		</svg>
		</a>
	</div>
	<div class="flex-ce m12">
		<button type="button" class="bg-primary ptb4 cf plr15 br8 f15" id="user-select">插入</button>
    </div>
    <div id="userlist" class="mb60">
    
    </div>
	<button type="button" class="weui-btn weui-btn_primary bottom-fixed" style="border-radius:0;background-color:#FF5722" onclick="javascript:location.hash='addUserPage'">+ 新增常用人</button>
</div>

<!--添加地址-->
<div id="_addUserPage" style="display:none">
	<div class="top flex-csb bg-ff plr12 ptb8">
		<a href="javascript:void(0)" onclick="javascript:location.hash='userList'">
		<svg class="icon" aria-hidden="true" color="#3ec77d">
		    <use xlink:href="#icon-zuojiantou"></use>
		</svg>
		</a>
		<span class="f15 fw-bold">新增常用人</span>
		<a href="#">
		<svg class="icon" aria-hidden="true" font-size="0.25rem" color="#3ec77d">
		    <use xlink:href="#icon-shouye-copy"></use>
		</svg>
		</a>
	</div>
    <form id="user_add_form" autocomplete="off">
    	<div class="user-list mtb12 bg-ff">
            <div class="input-bar flex-cs p8 bb-e5">
				<span class="flex-none">真实姓名</span>
				<input class="flex-auto h30 ta-right pl12 f14 username" type="text" placeholder="（与证件名字一致）" name="data[username]" cname="真实姓名" required>
			</div>
			<div class="input-bar flex-cs p8 pl4 bb-e5">
				<!-- <select class="cert-select-pane">
					<option value="1">身份证</option>
					<option value="2">护照</option>
					<option value="3">其他</option>
				</select> -->
				<span class="flex-none">身份证</span>
				<input class="flex-auto h30 ta-right pl12 f14 idcard" type="text" placeholder="（与证件号码一致）" name="data[code]" dtype="idnum" cname="身份证" required>
			</div>
			<div class="input-bar flex-cs p8 bb-e5">
				<span class="flex-none">电话号码</span>
				<input class="flex-auto h30 ta-right pl12 f14 phone" type="tel" placeholder="（必填）" name="data[mobile]" cname="电话号码" dtype="mobile" required>
			</div>
            <div class="input-bar flex-cs p8 bb-e5">
                <div class="weui-cell__bd">设为默认</div>
                <div class="weui-cell__ft">
                    <label for="add_switchCP" class="weui-switch-cp">
                    	<input type="hidden" id="add-default" name="state" value="0">
                        <input id="add_switchCP" class="weui-switch-cp__input" name="state" value="1" type="checkbox" />
                        <div class="weui-switch-cp__box"></div>
                    </label>
                </div>
            </div>
     	</div>
        <div class="weui-btn-area">
            <a class="weui-btn weui-btn_primary acf ajaxproxy"  href="<?php echo site_url('mobile/info/add')?>" formid="user_add_form" location="" callBack="add_info(data)">确认添加</a>
            <input type="reset" name="reset" style="display:none" />
        </div>
    </form>
</div>
<!--地址编辑-->
<div id="_editUserPage" style="display:none">
	<div class="top flex-csb bg-ff plr12 ptb8">
		<a href="javascript:void(0)" onclick="javascript:location.hash='userList'">
		<svg class="icon" aria-hidden="true" color="#3ec77d">
		    <use xlink:href="#icon-zuojiantou"></use>
		</svg>
		</a>
		<span class="f15 fw-bold">编辑常用人</span>
		<a href="/">
		<svg class="icon" aria-hidden="true" font-size="0.25rem" color="#3ec77d">
		    <use xlink:href="#icon-shouye-copy"></use>
		</svg>
		</a>
	</div>
    <form id="user_edit_form" autocomplete="off">
    	<div class="user-list mtb12 bg-ff">
            <div class="input-bar flex-cs p8 bb-e5">
				<span class="flex-none">真实姓名</span>
				<input class="flex-auto h30 ta-right pl12 f14 username" type="text" placeholder="（与证件名字一致）" name="data[username]" cname="真实姓名" required>
			</div>
			<div class="input-bar flex-cs p8 pl4 bb-e5">
				<!-- <select class="cert-select-pane">
					<option value="1">身份证</option>
					<option value="2">护照</option>
					<option value="3">其他</option>
				</select> -->
				<span class="flex-none">身份证</span>
				<input class="flex-auto h30 ta-right pl12 f14 idcard" type="text" placeholder="（与证件号码一致）" name="data[code]" dtype="idnum" cname="身份证" required>
			</div>
			<div class="input-bar flex-cs p8 bb-e5">
				<span class="flex-none">电话号码</span>
				<input class="flex-auto h30 ta-right pl12 f14 phone" type="text" placeholder="（必填）" name="data[mobile]" cname="电话号码" dtype="mobile" required>
			</div>
            <div class="input-bar flex-cs p8 bb-e5">
                <div class="weui-cell__bd">设为默认</div>
                <div class="weui-cell__ft">
                    <label for="edit_switchCP" class="weui-switch-cp">
                    	<input type="hidden" id="edit-default" name="state" value="0"> 
                        <input id="edit_switchCP" class="weui-switch-cp__input" name="state" value="1" type="checkbox" />
                        <div class="weui-switch-cp__box"></div>
                    </label>
                </div>
            </div>
			</div>
        <div class="weui-btn-area">
        	<input type="hidden" id="edit-id" name="id" value="0"> 
            <a class="weui-btn weui-btn_primary acf ajaxproxy"  href="<?php echo site_url('mobile/info/edit')?>" formid="user_edit_form" location="" callBack="edit_info(data)">确认编辑</a>
        </div>
    </form>
</div>
