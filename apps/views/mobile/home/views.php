<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/index'); echo template('mobile/header',$data);?>
<div class="top flex-csb bg-ff plr12 ptb8 mb15">
		<a href="javascript:history.back()">
			<svg class="icon c-diy" aria-hidden="true" ><use xlink:href="#icon-zuojiantou"></use></svg>
		</a>
		<span class="f15 fw-bold c-diy"><?php echo $item['title'];?></span>
		<a href="/">
			<svg class="icon c-diy" aria-hidden="true" font-size="0.25rem"><use xlink:href="#icon-shouye-copy"></use></svg>
		</a>
</div>
<div class="top bg-ff plr12 ptb15 mb15">
<!-- 	  <h3 class="f20 fw-bold ta-center bb-e5 pb10" ></h3> -->
      <div class="mt10">
        <?php echo $item['content'];?>
      </div>
</div>

<?php echo template('mobile/script');?>
<?php echo template('mobile/kf');?>
</body>
</html>