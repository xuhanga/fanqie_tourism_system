<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<template v-if="aditems">
		<van-swipe :autoplay="3000">
		  <van-swipe-item v-for="(v, index) in aditems" :key="index">
		  	<a :href="v.url">
		  		<img v-lazy="v.thumb" >
		  	</a>
		  </van-swipe-item>
		</van-swipe>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="ad_load"></van-loading></template>
	
	<van-row class="mt10">
		<van-col v-for="(v, index) in imenuitems" :key="index" span="6" class="text-center mb5">
			<div>
				<a :href="v.url"><img v-lazy="v.icon" class="img60"></a>
		  	</div>
			<div  v-text="v.tgname"></div>
		</van-col>
	</van-row>
	
</div>
<?php echo template('mobile/script');?>

<script>
	Vue.use(vant.Lazyload);
	new Vue({
		el: '#app',
		data: {
			aditems: '',
			ad_load:true,
			imenuitems:''
		},
	  	methods: {
	  	   load_ad:function(){
				var that = this;		  	   		
				axios.all([get_ad(),get_imenu()]).then(axios.spread(function (ad,imenu) {
					var aditem = ad.data,imenuitem = imenu.data;
					if(aditem.state==1){
			  			that.aditems = aditem.data;
				  	}else{
					  	that.ad_load = false;
				  		that.$toast(aditem.message);
					}

					if(imenuitem.state==1){
						that.imenuitems = imenuitem.data;
					}
					
				}));
		   }
	  	},
	  	mounted:function (){
	  		this.load_ad();
		}
	});

	function get_ad() {
		  return axios.post('/api/home/ad',Qs.stringify({cid:1}),ajaxconfig);
	}

	function get_imenu() {
		  return axios.get('/api/home/imenu',ajaxconfig);
	}
</script>
</body>
</html>