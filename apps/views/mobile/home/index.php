<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>

<div id="app">
	<div class="ct-flexbox text-center pb5 pt5" style="background-color: #f2f2f2">
		<div class="pr5 pl5 cr_888" @click="location"><p><van-icon name="location" class="f16" style="display:block;"></van-icon></p><p class="f12" v-text="l_name"></p></div>
		<div class="ct-flexbox-item pr5"><form action="/"><van-search v-model="skey" placeholder="你想去哪儿玩儿？" @search="onSearch" ></van-search></form></div>
	</div>
	<van-swipe :autoplay="3000" class="bg_ff">
	  <van-swipe-item v-for="(v, index) in aditems" :key="index" class="text-center">
	  	<a :href="v.url"><img v-lazy="v.thumb" ></a>
	  </van-swipe-item>
	</van-swipe>
		
	<template v-if="imenuitems">
		<van-row class="pt10 pb5 bg_ff" v-if="imenuitems.length">
			<van-col v-for="(v, index) in imenuitems" :key="index" span="6" class="text-center mb10">
				<div>
					<a :href="v.url"><img :src="v.icon" class="wd50"></a>
			  	</div>
				<div class="mt5" v-text="v.tgname"></div>
			</van-col>
		</van-row>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="ad_load"></van-loading></template>
	<!-- 精选线路 -->
	<template v-if="duan">
		<div class="bg_ff mt10 pt5 pb5" v-if="duan.length">
			<van-row>
				<van-col  span="24" class="text-center">
					<p class="f16 f_bold mb10"><span class="cr_red">短</span>线活动</p>
					<div class="van-hairline--top van-hairline--tops wd50 mauto"><span class="loadmore_line cr_888">5天以内 精选活动</span></div>
				</van-col>
			</van-row>
			<van-row class="p5" v-for="(v,index) in duan" :key="index">
				<a :href="'/mobile/tourism/detail/id-'+v.id" >
					<van-col  span="24" class="text-center">
						<img v-lazy="v.thumb" >
					</van-col>
					<van-col  span="24" class="pl5 pr5 pt5 lists-title f14">
						<van-col  span="18" >
							<div class="ellipsis" v-text="v.title"></div>
						</van-col>
						<van-col  span="6" >
							<div class="f12 lists-leader text-center">
								<img :src="v.leader.thumb" class="brus50" >
								<span v-text="v.leader.nickname" class="cr_888 ellipsis" ></span>
							</div>
						</van-col>
					</van-col>
					<van-col span="24">
						<div class="cr_888 pl5 pr5">
							<span  v-text="v.stime+'出发'"></span>&nbsp;
							<span  v-text="'难度:'"></span>
							<i :class="'iconfont icon-xingxing xingxing-'+v.diff" v-for="(vs, index) in diff" :key="index" v-if="index < v.diff"></i> 
							<span :class="'xingxing-'+v.diff" v-text="'（'+v.diff_say+'）'"></span>&nbsp;
						</div>
					</van-col>
				</a>
			</van-row>
		</div>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="ad_load"></van-loading></template>
	
	
	<template v-if="chang">
		<div class="bg_ff mt10 pt5 pb5" v-if="chang.length">
			<van-row>
				<van-col  span="24" class="text-center">
					<p class="f16 f_bold mb10"><span class="cr_red">长</span>线活动</p>
					<div class="van-hairline--top van-hairline--tops wd50 mauto"><span class="loadmore_line cr_888">5天以外 精选活动</span></div>
				</van-col>
			</van-row>
			<van-row class="p5" v-for="(v,index) in chang" :key="index">
				<a :href="'/mobile/tourism/detail/id-'+v.id" >
					<van-col  span="24" class="text-center">
						<img v-lazy="v.thumb" >
					</van-col>
					<van-col  span="24" class="pl5 pr5 pt5 lists-title f14">
						<van-col  span="18" >
							<div class="ellipsis" v-text="v.title"></div>
						</van-col>
						<van-col  span="6" >
							<div class="f12 lists-leader text-center">
								<img :src="v.leader.thumb" class="brus50" >
								<span v-text="v.leader.nickname" class="cr_888 ellipsis" ></span>
							</div>
						</van-col>
					</van-col>
					<van-col span="24">
						<div class="cr_888 pl5 pr5">
							<span  v-text="v.stime+'出发'"></span>&nbsp;
							<span  v-text="'难度:'"></span>
							<i :class="'iconfont icon-xingxing xingxing-'+v.diff" v-for="(vs, index) in diff" :key="index" v-if="index < v.diff"></i> 
							<span :class="'xingxing-'+v.diff" v-text="'（'+v.diff_say+'）'"></span>&nbsp;
						</div>
					</van-col>
				</a>
			</van-row>
		</div>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="ad_load"></van-loading></template>
	<van-popup v-model="l_content" position="bottom" >
	  <van-picker  show-toolbar :columns="columns" title="所在地选择"  @cancel="onCancel"  @confirm="onConfirm"></van-picker>
	</van-popup>
	<?php echo template('mobile/tabbar');?>
</div>


<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script>
//滚动公告
var v = new Vue({
	el: '#app',
	data: {
		diff:[1,2,3,4,5],
		aditems: '',
		ad_load:true,
		imenuitems:'',
		duan:'',
		chang:'',
		active:0,
		skey:'',
		l_content:false, columns:'',l_name:'<?php echo $l_name;?>',lid:<?php echo $lid;?>
	},
  	methods: {
  		location(){
  			var l = this.$toast.loading({duration: 0,mask: true,message: '加载中...'});
  			axios.post('/api/home/location', '',ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	    	this.columns = data.data;
  	  	  	    	this.l_content = true;
  	  	  	  	}else{
  	  	  	  	  	this.$toast(data.message);
  	  	  	  	}
  	  	    });
  	  	},
    	onConfirm(value, index) {
  	    	location.href = "/mobile/home/index/lid-"+value.id+'.html';
    	},
    	onCancel() {
    		this.l_content = false;
    	},
  	   	onSearch(){
  	  	  if(this.skey){
    	  	location.href = "/mobile/tourism/search/key-"+this.skey;
  	  	  }else{
			this.$toast.fail('请输入内容');
  	  	  }
  	  		
  	   	},
  	   	load_ad:function(){
			var that = this;
			//加载全部ajax		  	   		
			axios.all([get_ad(),get_imenu()]).then(axios.spread(function (ad,imenu) {
				var aditem = ad.data,imenuitem = imenu.data;
				if(aditem.state==1){
		  			that.aditems = aditem.data;
			  	}else{
				  	that.ad_load = false;
				}
				if(imenuitem.state==1){
					that.imenuitems = imenuitem.data;
				}
				
			}));
	   	}
  	},
  	mounted:function (){
  	  	this.load_ad();
  	  	if(this.lid){
  	  	  	get_tourism(this.lid);
  	  	}
	}
});

function get_ad() {//获取广告
	  return axios.post('/api/home/ad',Qs.stringify({cid:1}),ajaxconfig);
}

function get_imenu() {//获取菜单
	  return axios.get('/api/home/imenu',ajaxconfig);
}

function get_tourism(lid) {//获取旅游活动
	axios.post('/api/home/tourism',Qs.stringify({lid:lid}),ajaxconfig).then((response)=> {
  	  	var data = response.data;
		if(data.state==1){
			v.duan = data.data.duan?data.data.duan:[];
			v.chang = data.data.chang?data.data.chang:[];
		}else{
			v.duan = [];
			v.chang = [];
		}
    });
}

</script>
</body>
</html>