<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<style>
  .layui-laydate-main{width: 100%;}.laydate-day-mark{color: #5fb878}
  .layui-laydate-content td, .layui-laydate-content th{width:1%;}
  .layui-laydate, .layui-laydate-hint{box-shadow:none;}
</style>
<div id="app">
	<van-row class="bg_ff">
		<van-col span="24" class="text-center f24 pt10 pb10" v-text="'选择拼团时间'"></van-col>
		<van-col span="24" class="text-center p5">
			<div id="time"> </div>
		</van-col>
		<van-col span="24" class="text-center pt10 pb10 cr_888" v-text="'显示 ‘Go’ 字代表已经有人开启，可以直接参与'"></van-col>
	</van-row>
	<van-goods-action>
		<van-goods-action-mini-btn icon="home" text="返回" @click="onClickLeft" ></van-goods-action-mini-btn>
	  	<van-goods-action-big-btn text="下一步，选时间"  primary  @click="onSub"></van-goods-action-big-btn>
	</van-goods-action>
</div>
<div style="display: none;">
	<form id="sub">
		<input type="hidden" name="data[id]" value="<?php echo $item['id']?>" required>
		<input type="hidden" id='stime' name="data[stime]" cname="选择时间" required>
		<input type="hidden" name="data[day_num]" value="<?php echo $item['day_num']?>" required>
	</form>
</div>
<?php echo template('mobile/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'jquery.min.js'?>" ></script>
<script type="text/javascript" src="<?php echo JS_PATH.'mobile/laydate.js'?>" ></script>
<?php $mark = '{}'; if($items){foreach ($items as $v){ $news[$v['stime']] = 'Go';}  $mark = json_encode($news);}?>
<script>
new Vue({
	el: '#app',
	data: {
		
	},
  	methods: {
  		onSub(){
  	  		sub();
  	    }
  	},
  	mounted:function (){
  	  	
	}
});
function sub(){
	vant.Toast.loading({message: '加载中...'});
	$.ajax({
		url:'/api/pt/creat',
		type:'post',
		data:$('#sub').serialize(),
		dataType:'json',
		success:function(d){
			if(d.state==1){
				location.href = d.data;
			}else{
				vant.Toast(d.message);
			}
		}
	});
};
laydate.render({
  elem: '#time',
  position: 'static',
  mark: <?php echo $mark;?>,
  min: 7,
  btns: false,
  showBottom:false,
  done: function(value, date){
	if(value in this.mark){
		$('#stime').val('');
		var l = vant.Toast.loading({message: '加载中...'});
		$.post('/api/pt/get_time',{time:value,id:<?php echo $item['id']?>},function(d){
				l.clear();
				if(d.state==1){
					vant.Dialog.confirm({
						  title:'温馨提示',
						  confirmButtonText:'前往报名',
						  message: '<p class="f12 text-center">您选择的 '+ d.data.time +' 路线已有可报名活动 </p> <p class="f12 text-center">剩余名额' + d.data.num +'人</p>'
						}).then(() => {
							location.href = d.data.url;
						}).catch(() => {});
				}else{
					vant.Toast(d.message);
				}
			},'json');
		$('.layui-this').removeClass('layui-this laydate-selected');
	}else{
		var day_num = '<?php echo $item['day_num']?>';
		$('#stime').val(value);
		if(day_num > 1){
			var startTime = value;
			var endTime = add_time(startTime,day_num);
			var that = '#time';
			tds = lay(that).find('td');
			tds.removeClass('laydate-selected');
			lay.each(tds, function(i, item){
			     var ymd = checkDate(lay(item).attr('lay-ymd'))
			     if(ymd >= startTime){
				     if(ymd == endTime){
				    	 lay(item).addClass('layui-this laydate-selected');
						 return true;
					 }else{
						 lay(item).addClass('laydate-selected');
					 }
				 }
			});
		}
	}
  }
});
function checkDate(t) {
    var myDate = new Date(t.replace(/-/g,"/"));
    var mydate = myDate.getDate();
    if(myDate.getDate()<10){
        d = '0'+ myDate.getDate();
    }else{
    	d = myDate.getDate();
    }
    if((myDate.getMonth() + 1) < 10){
        m = '0' + (myDate.getMonth() + 1);
    }else{
    	m = myDate.getMonth() + 1;
    }
    var today = myDate.getFullYear() + '-' + m + '-' + d;
    return today;
}

function add_time(t,num){
	timeadd = new Date(t.replace(/-/g,"/"));
	timeadd = new Date(timeadd.getTime() + (num-1)*24*60*60*1000); 
	timeadd = timeadd.getFullYear() + "-" + (timeadd.getMonth() + 1) + "-"+ timeadd.getDate(); //重新格式化
	return checkDate(timeadd);
}

</script>
</body>
</html>