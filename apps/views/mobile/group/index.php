<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<template v-if="tag">
		<van-row class="pt10 pb5 bg_ff group-tag">
			<van-col v-for="(v, index) in tag" :key="index" span="5" class="text-center mb10">
				<a :href="'/mobile/group/cat_lists/cid-1-id-'+v.id">
					<div>
						<img :src="v.thumb" class="wd50">
				  	</div>
					<div  v-text="v.tname" class="mt5"></div>
				</a>
			</van-col>
		</van-row>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="ad_load"></van-loading></template>
	
	<van-cell-group class="mt10 group-tag">
	  <van-cell ><template slot="title"><van-tag mark >探索难度</van-tag></template></van-cell>
	  <van-cell >
	  	<van-row class="pt10 pb5 bg_ff diff">
			<van-col v-for="(v, index) in diff" :key="index" span="5" class="text-center mb5">
				<a :href="'/mobile/group/cat_lists/cid-2-id-'+index">
					<div>
						<i :class="'iconfont f30 icon-xing'+index+' gicon'+index"></i>
				  	</div>
					<div class="mt5" v-text="v"></div>
				</a>
			</van-col>
		</van-row>
	  </van-cell>
	</van-cell-group>
	<van-cell-group class="mt10 group-tag">
	  <van-cell ><template slot="title"><van-tag mark>时间跨度</van-tag></template></van-cell>
	  <van-cell >
	  	<van-col v-for="(v, index) in day" :key="index" span="5" class="text-center mb5">
	  			<a :href="'/mobile/group/cat_lists/cid-3-id-'+v.id">
					<div>
						<van-circle v-model="v.num" :rate="v.rate" color="#13ce66"  fill="#fff"  size="50px"  layer-color="#eee"  :text="v.text"  :speed="100" :stroke-width="60" class="f_bold f20"></van-circle>
			  		</div>
					<div  v-text="v.name"></div>
				</a>
		</van-col>
	  </van-cell>
	</van-cell-group>
	<?php echo template('mobile/tabbar');?>
</div>


<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script>
//滚动公告
new Vue({
	el: '#app',
	data: {
		active:1,
		tag:<?php echo $tag;?>,
		diff:<?php echo $diff;?>,
		day:<?php echo $day;?>,
	},
  	methods: {
  	   
  	},
  	mounted:function (){
	},
  	ready:function(){
  		
  	}
});
</script>
</body>
</html>