<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="旅游列表"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<template v-if="lists">
		<div class="bg_ff mt10 pt5 pb5">
			<van-row class="p5" v-for="(v,index) in lists" :key="index">
				<a :href="'/mobile/tourism/detail/id-'+v.id" >
					<van-col  span="24" class="text-center">
						<img v-lazy="v.thumb" >
					</van-col>
					<van-col  span="24" class="pl5 pr5 pt5 lists-title f14">
						<van-col  span="18" >
							<div class="ct-flexbox-item ellipsis" v-text="v.title"></div>
						</van-col>
						<van-col  span="6" >
							<div class="f12 lists-leader text-center">
								<img :src="v.leader.thumb" class="brus50" >
								<span v-text="v.leader.nickname" class="cr_888 ellipsis" ></span>
							</div>
						</van-col>
					</van-col>
					<van-col span="24">
						<div class="cr_888 pl5 pr5">
							<span  v-text="v.stime+'出发'"></span>&nbsp;
							<span  v-text="'难度:'"></span>
							<i :class="'iconfont icon-xingxing xingxing-'+v.diff" v-for="(vs, index) in diff" :key="index" v-if="index < v.diff"></i> 
							<span :class="'xingxing-'+v.diff" v-text="'（'+v.diff_say+'）'"></span>&nbsp;
						</div>
					</van-col>
				</a>
			</van-row>
		</div>
	</template>
	<template v-else><p class="cr_hs2 text-center mt60"><i class="iconfont icon-meiyoujieguo f80 d_block"></i><span v-text="'什么也木有~'"></span></p></template>
</div>


<?php echo template('mobile/script');?>
<script>
//滚动公告
new Vue({
	el: '#app',
	data: {
		cid:<?php echo $cid;?>,
		id:<?php echo $id;?>,
		diff:[1,2,3,4,5],
		ad_load:true,
		lists:''
	},
  	methods: {
  	   load_ad:function(){
			var that = this;
			//加载全部ajax	
			var l = this.$toast.loading({duration: 0,mask: true,message: '加载中...'});	  	   		
			axios.all([get_tourism(that.cid,that.id)]).then(axios.spread(function (jx_tm) {
				var jxitem = jx_tm.data;
				if(jxitem.state==1){
					that.lists = jxitem.data;
				}
				l.clear();
			}));
	   }
  	},
  	mounted:function (){
  		this.load_ad();
	}
});


function get_tourism(cid,id) {//获取旅游活动
	  return axios.post('/api/home/cat_lists',Qs.stringify({cid:cid,id:id,lid:<?php echo $U['location']?>}),ajaxconfig);
}
</script>
</body>
</html>