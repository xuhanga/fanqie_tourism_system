<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?php echo $web_name;?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />
	
	<link rel="stylesheet" href="http://at.alicdn.com/t/font_593212_qd9cp0eunpix80k9.css">
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/vant.min.css";?>">
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/style.css";?>">
	<?php  if(isset($definedcss)){   foreach ($definedcss as $v){?>
	<link type="text/css" href="<?php echo $v.'.css'?>" rel="stylesheet" />
	<?php }}?>
</head>
<body>