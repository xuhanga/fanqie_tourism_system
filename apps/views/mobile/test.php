<script type="text/javascript" src="<?php echo JS_PATH.'jquery.min.js'?>" ></script>
<script src="<?php echo JS_PATH.'jweixin-1.2.0.js'?>"></script>
<script type="text/javascript">
wx.config(<?php echo $wxconfig; ?>);
wx.ready(function () {
	<?php if(!$this->session->city){?>
    wx.getLocation({
    	type: 'wgs84', // 可传入'gcj02'
    	success: function (res) {
	    	var latitude = res.latitude; 
	    	var longitude = res.longitude;
	    	$.post('/wechat/test/jx',{la:latitude,lg:longitude},function(d){
		    	if(d.state){
			    	<?php if(isset($home)){?>
		    		 location.reload();
		    		 <?php }?>
			    }
			},'json');
    	}
 	});
 	<?php }?>
});
</script>

