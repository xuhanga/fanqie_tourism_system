<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/swiper.min'); echo template('mobile/header',$data); ?>
<div id="app">
	<van-row class="bg_ff td_header cr_ff">
		<van-col span="24" class="text-center"><a  :href="photo?'/mobile/photo/lists/id-'+item.id:'#'"><img v-lazy="item.thumb" ></a></van-col>
		<template v-if="photo">
			<van-col  span="24" class="img_num text-right f14 p10">
				<a  :href="'/mobile/photo/lists/id-'+item.id"><span><van-icon name="photo" class="mr5" ></van-icon><cc v-text="photo+'张'"></cc></span></a>
			</van-col>
		</template>
		<van-col span="24" class="title f16 p10 ellipsis"><p v-text="item.title" ></p></van-col>
	</van-row>
	
	<van-row class="bg_ff mb10 p10 f14">
		<van-col span="24">
			<div class="ct-flexbox"><img :src="leader.thumb" class="img_wd50 brus50">
				<div class="ct-flexbox-item ml10" >
					<p v-text="leader.nickname"></p>
					<p class="cr_888 mt5" ><i class="iconfont icon-lingyu f12 leader-icon-color " v-text="' 活动领队'"></i></p>
				</div>
			</div>
		</van-col>
		<van-col span="24" class="mt10" >
			<span v-text="'难度：'"></span>
			<i :class="'iconfont icon-xingxing xingxing-'+item.diff" v-for="(v, index) in diff" :key="index" v-if="index<item.diff"></i> 
			<span :class="'xingxing-'+item.diff" v-text="'（'+item.diff_say+'）'"></span> 
			<span class="cr-alreadygo" v-text="item.integral_plus_say"></span>
		</van-col>
		<van-col span="24" class="mt5">
			<div class="ct-flexbox"><div class="ct-flexbox-item" v-text="'集合：'+item.venue"></div><span class="cr_red" v-text="item.money"></span><span v-text="'元/人'"></div>
		</van-col>
	</van-row>
	
	<van-cell-group class="mb10 td_gotime">
	  	<van-cell title="出发时间" value="我要定制" is-link url="/mobile/pt/index.html"></van-cell>
	  	<div class="van-cell swiper">	
			<div class="swiper-container">
			    <div class="swiper-wrapper">
			    	<template v-for="(v, indexs) in gotime" :key="indexs">
				    	<div class="swiper-slide text-center" :class="v.selected" v-on:click="go_time(indexs,v.id,v.state)">
				        	<div class="activity-content" >
					        	<p v-text="v.times"></p>
					        	<p v-html="v.state_html"></p>
					        	<div class="checked-square"></div>
					        	<div class="checked-icon flex-center"><van-icon name="success" ></van-icon></div>
				        	</div>
				        	<div class="right-lian">
								<div class="left-circle"></div><div class="right-circle"></div><div class="left-center-bg"></div><div class="right-center-bg"></div><div class="small-left-circle"></div><div class="small-right-circle"></div><div class="left-center-line"></div><div class="right-center-line"></div>
							</div>
				        </div>
			    	</template>
			    	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="load"></van-loading></template>
			    </div>
			</div>
		</div>
	</van-cell-group>
	
	<template v-if="to_user">
	<van-cell-group class="mb10">
	  	<van-cell :title="'已参与的人('+sign_num+'人)'" :value="'总共'+full+'人'" is-link></van-cell>
	  	<van-cell>
	  		<van-row class="text-center">
	  			<van-col span="4" class="leader">
	  				<div class="cy-user">
	  					<img :src="leader.thumb" class="img_wh40" >
	  					<div class="cy-num" v-text="leader_num"></div>
	  				</div>
	  				<i class="iconfont icon-lingyu leader-icon"></i>
	  				<span v-text="leader.nickname"></span>
	  			</van-col>
	  			<van-col span="4" v-for="(v, indexs) in to_user" :key="indexs">
	  				<div class="cy-user">
	  					<img :src="v.thumb" class="img_wh40" >
	  					<div class="cy-num" v-text="v.num"></div>
	  				</div>
	  				<span v-text="v.nickname"></span>
	  			</van-col>
	  		</van-row>
	  	</van-cell>
	</van-cell-group>
	</template>
	<template v-else><van-loading type="spinner" color="black" class="loading" v-show="load"></van-loading></template>
	
	<van-tabs active="0" sticky class="bg_ff mb60">
		<van-tab title="活动详情" class="td_content">
			<p v-html="item.content" ></p>
		</van-tab>
		<van-tab title="行程准备" class="td_content">
			<p v-html="item.journey"  ></p>
		</van-tab>
		<van-tab title="费用说明" class="td_content">
			<p v-html="item.money_say" ></p>
		</van-tab>
		<van-tab title="评价">
			无
		</van-tab>
	</van-tabs>
	<van-popup v-model="qun_show" class="p5"><img v-lazy="qun" ></van-popup>
	<van-goods-action>
		<van-goods-action-mini-btn icon="home" text="更多活动" url="/mobile/home/index.html" ></van-goods-action-mini-btn>
	  	<van-goods-action-mini-btn icon="chat" text="微信群" @click="onQun" ></van-goods-action-mini-btn>
	  	<van-goods-action-big-btn :text="sign_btn.name"  primary  :url="sign_btn.url" v-bind:style='sign_btn_style' ></van-goods-action-big-btn>
	</van-goods-action>
</div>


<?php echo template('mobile/script');?>
<?php echo template('mobile/share');?>
<script src="<?php echo JS_PATH.'jquery.min.js'?>"></script>
<script src="<?php echo JS_PATH.'mobile/lazyload.min.js'?>"></script>
<script src="<?php echo JS_PATH.'mobile/swiper.min.js'?>" ></script>
<script>

var gotime = <?php echo json_encode($gotime);?>;
new Vue({
	el: '#app',
	data: {
		item:'',gotime:gotime,id:<?php echo $id;?>,to_user:'',sign_num:0,
		full:0,leader:'',leader_num:0,load:true,sign_btn:'',sign_btn_style:'',
		diff:[1,2,3,4,5],photo:'',qun_show:false,qun:''
	},
  	methods: {
  	  	onQun(){
  	  	  	this.qun_show = true;
  	  	},
 		 go_time:function(index,cid,state){
 	 		this.to_user = '';
  			this.gotime.forEach(function(obj){//先取消全部选中
                obj.selected = '';
            });
  			this.gotime[index].selected = 'selected';
  			this.sign_class(state,cid);
  			//选择时间后获取数据
  			var l = vant.Toast.loading({duration: 0,mask: true,message: '加载中...'});
  			axios.all([tourism_order(this.id,cid)]).then(axios.spread((torder)=> {
				this.to_info(torder.data);
				l.clear();
			}));
 	 	 },
 	 	 to_info:function(data){//处理选择时间后的数据
  	 		if(data.state==1){
				this.to_user = data.data.order_user;
				this.sign_num = data.data.sign_num;
				this.full = data.data.full;
				this.leader = data.data.leader.user;
				this.leader_num = data.data.leader.num;
				this.qun = data.data.qun;
			}else{
				this.$toast(data.message);
			}
 	 	 },
 	 	 sign_class:function(state,cid){
 	 	 	 if(state==1){
  	 	 		this.sign_btn = {'name':'已满员','url':'#'};
  	  	 	 	this.sign_btn_style = {'pointer-events':'none'};
 	 	 	 }else if(state==2){
  	 	 		this.sign_btn = {'name':'活动已结束','url':'#'};
  	  	 	 	this.sign_btn_style = {'pointer-events':'none'};
 	 	 	 }else{
  	 	 		this.sign_btn = {'name':'立即报名','url':'/mobile/tourism/order/id-'+this.id+'-cid-'+cid};
  	  	 	 	this.sign_btn_style = '';
 	 	 	 }
 	 	 }
  	},
  	mounted:function (){
  		var l = vant.Toast.loading({duration: 0,mask: true,message: '加载中...'});
  		var swiper = new Swiper('.swiper-container', {
  			width:150,
  	        initialSlide:<?php echo $init['selected_init'];?>,
  		});
  		axios.all([tourism_order(this.id,<?php echo $init['init_cid'];?>),tourism_photo(this.id),tourism_detail(this.id)]).then(axios.spread( (torder,photo,detail)=> {
			this.to_info(torder.data);
			this.photo = photo.data.data;
			this.item = detail.data.data;
			l.clear();
		}));
  		this.sign_class(<?php echo $init['state'];?>,<?php echo $init['init_cid'];?>);
	}
});

function tourism_order(id,cid) {
	  return axios.post('/api/home/tourism_order',Qs.stringify({id:id,cid:cid}),ajaxconfig);
}

function tourism_photo(id) {
	  return axios.post('/api/home/tourism_photo',Qs.stringify({id:id}),ajaxconfig);
}
function tourism_detail(id) {
	  return axios.post('/api/home/tourism_detail',Qs.stringify({id:id}),ajaxconfig);
}

$("img").each(function () {
	var oImgSrc = $(this).attr("src");
	$(this).attr("src", "").attr("data-original", function () {
		return oImgSrc;
	}); 
});
$("img").lazyload({effect: "fadeIn"});
</script>

</body>
</html>