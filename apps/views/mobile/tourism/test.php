<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	
	<div class="mt10 ml10">
		<van-stepper ></van-stepper>
	</div>
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
});

</script>
</body>
</html>