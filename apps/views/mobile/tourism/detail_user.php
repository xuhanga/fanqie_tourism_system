<?php  $data['definedcss'] = array(CSS_PATH.'mobile/index',CSS_PATH.'mobile/mall'); echo template('mobile/header',$data);?>
<div class="top flex-csb bg-ff plr12 ptb8">
		<a href="javascript:history.back()">
			<svg class="icon c-diy" aria-hidden="true"><use xlink:href="#icon-zuojiantou"></use></svg>
		</a>
		<span class="f15 fw-bold">已参加的人</span>
		<a href="/"></a>
</div>

<div class="bg-ff plr15" style="margin-top: 10px;">
	<p class="ptb12 f14 bb-e5">领队</p>
	<div class="flex-cs r-wrap">
	<?php $leader = json_decode($item['leader'],true);if($leader)foreach ($leader as $v){?>
	
		<a href="#" class="flex-dcc ptb12 w20">
			<div class="w70">
				<img src="<?php echo $v['thumb'];?>" alt="" class="w100 h100 br-circle">
			</div> 
			<span class="text-hidden f12 w90 ta-center"><?php echo $v['nickname'];?></span>
		</a>
	<?php } ?>
	</div>
</div>
<div class="bg-ff plr15 mtb15">
	<p class="ptb12 f14 bb-e5">队员</p>
	 <?php if($items){ ?>
		<div class="flex-cs r-wrap">
	       	<?php foreach ($items as $vs){ ?>
					<a href="#" class="flex-dcc ptb12 w20">
						<div class="w70 p-r br-circle" style="overflow:hidden">
							<img src="<?php echo $vs['thumb'];?>" alt="" class="w100 h100 br-circle">
							<!-- 不止一人情况 -->
							<div class="d-extra-pane w100"><?php echo $vs['num'];?></div>
						</div> 
						<span class="text-hidden f12 w90 ta-center"><?php echo $vs['nickname'];?></span>
					</a>
			<?php } ?>
		</div>
	<?php }?>			
</div>

<?php echo template('mobile/script');?>
<?php echo template('mobile/footer');?>