<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="个人资料编辑"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	
    <van-cell-group >
  		<van-field  v-model="nickname" clearable  label="昵称"  placeholder="请输入昵称"   ></van-field>
		<van-field  v-model="username" clearable  label="姓名"  placeholder="请输入姓名"   ></van-field>
		<van-field  v-model="mobile"  clearable  label="手机号" type="number"  placeholder="请输入手机号" ></van-field>
		<van-field  v-model="w_id"  clearable  label="微信号"  placeholder="提现时候会使用"></van-field>
	</van-cell-group>
	<van-button type="primary" bottom-action class="van-contact-list-bottom" @click="sub()" text="提交编辑"></van-button> 
</div>

<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		nickname:'<?php echo $U['nickname']?>',
		username:'<?php echo $U['username']?>',
		mobile:'<?php echo $U['mobile']?>',
		w_id:'<?php echo $U['w_id']?>'
	},
  	methods: {
  		sub(){
  	  		if(this.nickname&&this.username&&this.mobile&&this.w_id){
  	  	  		
  	  	  		var data = {"data[username]":this.username,"data[nickname]":this.nickname,"data[mobile]":this.mobile,"data[w_id]":this.w_id};
  	  	  	  	var l = this.$toast.loading({duration: 0,mask: true,message: '提交中...'});
  				axios.post('/mobile/user/edit', Qs.stringify(data),ajaxconfig).then((response)=> {
  		  	  	  	var data = response.data;
  		  	  	  	l.clear();
  		  	      	if(data.state==1){
  		  	  	    	location.href = "/mobile/user/index.html";
  			  	  	    this.$toast(data.message);
  		  	  	  	}else{
  			  	  	  	this.$toast(data.message);
  				  	}
  		  	    });
  	  	  	}else{
  	  	  	  	this.$toast.fail('以上都是必填');
	  	  		return ;
  	  	  	}
  	  	  	
  	  	}
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>