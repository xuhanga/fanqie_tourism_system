<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $data['definedcss'] = array(CSS_PATH.'mobile/swiper-3.3.1.min',CSS_PATH.'mobile/sweetalert2.min',CSS_PATH.'mobile/index'); echo template('mobile/header',$data);?>
<div class="top flex-csb bg-ff plr12 ptb8 mb15">
		<a href="javascript:history.back()">
			<svg class="icon c-diy" aria-hidden="true" ><use xlink:href="#icon-zuojiantou"></use></svg>
		</a>
		<span class="f15 fw-bold c-diy">商业合作</span>
		<a href="/">
			<svg class="icon c-diy" aria-hidden="true" font-size="0.25rem"><use xlink:href="#icon-shouye-copy"></use></svg>
		</a>
</div>
<div class="top bg-ff plr12 ptb15 mb60">
      <div class="">
        <?php echo $item['content'];?>
      </div>
</div>
<form id="profile_form" class="flex-dcc">
   	<ul class="bg-ff plr12 w100">
      <li class="flex-csb ptb15 bb-e5"">
        <span class="flex-none">姓名</span>
        <div class="flex-ce fg1">
          <input type="text" name="data[username]" class="fg1 f15 h30 ta-right" cname="姓名" required>
        </div>
      </li>
      <li class="flex-csb ptb15 bb-e5">
        <span class="flex-none">手机</span>
        <div class="flex-ce fg1">
          <input type="text" name="data[mobile]" dtype="mobile" cname="手机号" class="fg1 f15 h30 ta-right" required>
        </div>
      </li>
      <li class="flex-csb ptb15 bb-e5">
        <span class="flex-none">合作阐述</span>
        <div class="flex-ce fg1">
          <input type="tel" name="data[content]"  class="fg1 f15 h30 ta-right">
        </div>
      </li>
    </ul>
    <a class="bottom-fixed bg-primary f16 acf ta-center ptb12 ajaxproxy" formId="profile_form" href="<?php echo site_url('mobile/user/business')?>" location="<?php echo site_url('mobile/user/index')?>">提交保存</a>
</form>
<?php echo template('mobile/script');?>
</body>
</html>