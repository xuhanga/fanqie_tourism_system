<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="申请团长"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
    <van-row class="p5 bg_ff">
	  <van-col span="24">
	  	<?php echo $item['content'];?>
	  </van-col>
	</van-row>
	
	<template v-if="fx_state==0">
	    <van-button type="primary" bottom-action class="van-contact-list-bottom" @click="fx_state==0&&apply()" text="申请成为团长"></van-button> 
	</template> 
	<template v-else-if="fx_state==1">
		<van-button type="primary" bottom-action class="van-contact-list-bottom" text="正在审核中"></van-button>  
	</template>
	<template v-else-if="fx_state==2">
		<van-button type="primary" bottom-action class="van-contact-list-bottom" text="你的申请被拒绝"></van-button>  
	</template>
	<template v-else-if="fx_state==3">
		<van-button type="primary" bottom-action class="van-contact-list-bottom" @click="fx" text="进入团队系统"></van-button>  
	</template>
</div>
<div class="mb60">&nbsp;</div>


<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	data: {
		fx_state:<?php echo $user['fx'];?>
	},
  	methods: {
  		apply(){
  			var l = this.$toast.loading({duration: 0,mask: true,message: '申请中...'});
  			axios.post('/mobile/user/fx_apply', '',ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	l.clear();
  	  	      	if(data.state==1){
  	  	  	    	this.fx_state = 1;
  	  	  	  	}
  	  	  	    this.$toast(data.message);
  	  	    });
  	  	},
  	  	fx(){
  	  	  	location.href = "/mobile/fx/index.html";
  	  	}  	  	
  	},
  	mounted:function (){
  		
	}
});
</script>
</body>
</html>