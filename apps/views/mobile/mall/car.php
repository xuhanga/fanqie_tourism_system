<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php echo template('mobile/header');?>
<div id="app">
	<van-nav-bar title="购物车"  left-arrow  @click-left="onClickLeft"  @click-right="onClickRight" class="mb10">
	  <van-icon name="wap-home" slot="right" />
	</van-nav-bar>
	<van-checkbox-group class="card-goods"  v-model="checkedGoods">
		<van-cell v-for="(item,index) in goods"    >
	      <van-checkbox ref="checkboxes" class="card-goods__item" :name="item.rid">
	        <van-card   :title="item.name"  :desc="item.options"  :num="item.qty"  :price="formatPrice(item.price)" :thumb="item.thumb" ></van-card>
	      </van-checkbox>
	    </van-cell>
    </van-checkbox-group>
    <van-submit-bar :price="totalPrice" :disabled="!checkedGoods.length" :button-text="submitBarText"  @submit="onSubmit"    >
    	<van-icon v-if="checkedGoods.length" name="delete ml10 cr-full" @click="del"></van-icon>
    </van-submit-bar>
</div>
<?php echo template('mobile/script');?>
<script type="text/javascript">
new Vue({
	el: '#app',
	data: {
		checkedGoods: <?php echo $ckarr;?>,
	    goods: <?php echo $items?json_encode($items):'[]';?>
	},
	computed: {
	    submitBarText() {
	      const count = this.checkedGoods.length;
	      return '结算' + (count ? `(${count})` : '');
	    },
	    totalPrice() {
	    	return this.goods.reduce((total, item) => total + (this.checkedGoods.indexOf(item.rid) !== -1 ? item.price : 0), 0);
	    }
  	},
  	methods: {
  		formatPrice(price) {
        	return (price / 100).toFixed(2);
      	},
  		toggle(index) {
      		console.log(index)
    	      this.$refs.checkboxes[index].toggle();
    	},
  	    onSubmit() {
    		var l = this.$toast.loading({duration: 0,mask: true,message: '结算中...'});
  			axios.post('/mobile/mall/save_cart', Qs.stringify({ids:this.checkedGoods}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	if(data.state==1){
	  	  	  	  	  location.href = "/mobile/mall/order.html";
  	  	  	  	}
  	  	  	  	l.clear();
  	  	  	    this.$toast(data.message);
  	  	    });
  	    },
  	    del(){
  	    	var l = this.$toast.loading({duration: 0,mask: true,message: '添加中...'});
  			axios.post('/mobile/car/del', Qs.stringify({data:this.checkedGoods}),ajaxconfig).then((response)=> {
  	  	  	  	var data = response.data;
  	  	  	  	if(data.state==1){
	  	  	  	  	  for(var i=0;this.checkedGoods.length>i;i++){
	      	  	  	  	this.goods = this.goods.filter(item => item.rid !== this.checkedGoods[i]);
	    	  	  	  }
      	    		  this.checkedGoods = [];
  	  	  	  	}
  	  	  	  	l.clear();
  	  	  	    this.$toast(data.message);
  	  	    });
  	  	}
  	}
});
</script>
</body>
</html>