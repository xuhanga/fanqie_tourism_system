<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 旅游
 * @author chaituan@126.com
 */
class TourismTime_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_time';
	}
	
	function quicksave($data) {
		$counter = 0;
		// 保存数据
		foreach ( $data ['hids'] as $key => $id ) {
			if ($this->updates( $data ['data'] [$key], "id=" . $id )) {
				$counter ++;
			}
		}
		// 只要一条数据保存成功，则该操作成功
		if ($counter > 0) {
			AjaxResult_ok();
		} else {
			AjaxResult_error();
		}
	}
}