<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 点赞管理
 * @author chaituan@126.com
 */
class Star_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'star';
	}
}