<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 标签
 * @author chaituan@126.com
 */
class TourismTag_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_tag';
	}
	
	// 缓存分类
	function cache() {
		$items = $this->getItems('','','sort');
		set_Cache('tourism_tag',$items);
	}
}