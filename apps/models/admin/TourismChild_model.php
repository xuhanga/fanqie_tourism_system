<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
/**
 * 子活动
 * @author chaituan@126.com
 */
class TourismChild_model extends MY_Model {
	public function __construct() {
		parent::__construct ();
		$this->table_name = 'tourism_child';
	}
}