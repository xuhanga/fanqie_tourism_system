<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 常用联系人
 * @author chaituan@126.com
 */
class Info_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'info';
	}
}