<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Order extends WechatCommon {
	
	function __construct(){
		parent::__construct();
		$this->load->model('admin/Order_model');
	}
	
//全部订单列表 1待付款   2(已支付)待发货   3(已发货)待收货   4(收到货)待评价 5退货  6 订单关闭
	function lists(){
		$id = Gets('id','num');
		$where = "";
		if($id){
			$where = "and state=$id";
		}else{
			$id="";
		}
		$uid = $this->User['id'];
		$data['newItems'] = $this->Order_model->getItems("state<>1 and uid=$uid $where","", 'id desc');
		$data['xz'] = $id;
		$this->load->view('mobile/order/lists',$data);
	}
	//订单详情
	function detail(){
		$id = Gets('id','checkid');
		$uid = $this->User['id'];
		if($id){
			$data['newItems'] = $this->Order_model->getItem("uid=$uid and id=$id","");
			$this->load->view('mobile/order/detail',$data);
		}else{
			showmessage('页面错误','error');
		}
	}
	//确认收货
	function affirm_receive(){
		if(is_ajax_request()){
			$uid = $this->User['id'];
			$id = Gets('id','checkid');
			$order = $this->Order_model->getItem("id=$id and uid=$uid and state=3",'prices,order_no');
			$this->Order_model->start();
			if(!$order)AjaxResult_error("非法操作");
			$time = time();
			//开始计算收益
			$this->User_model->fx_fl($order['prices'],$uid);
			//更新状态和收货时间
			$result = $this->Order_model->updates(array('state'=>6),"id=$id and uid=$uid and state=3");
			is_AjaxResult($result);
		}else{
			showmessage('页面错误','error');
		}
	}
	
	function back(){
		$id = Gets('id','num');
		$uid = $this->User['id'];
		$result = $this->Order_model->updates(array('state'=>5),"id=$id and uid=$uid and state=3");
		is_AjaxResult($result);
	}
}
