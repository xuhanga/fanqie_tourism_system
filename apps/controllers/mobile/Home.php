<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Home extends WechatCommon {
	function __construct(){
		parent::__construct();
		$this->load->vars('icon_xz',1);
	}
	
	public function index() {
		$lid = Gets('lid');
		$dw = dingw($lid);
		if($lid){
			$data['l_name'] = $dw['aname'];
			$this->load->model('admin/User_model','user');
			$this->user->updates_se(array('location'=>$lid),array('id'=>$this->User['id']));
			$data['lid'] = $dw['id'];
		}else{
			$data['l_name'] = $dw['aname'];
			$data['lid'] = $dw['id'];
		}
		$this->load->view('mobile/home/index',$data);
	}
	
}
