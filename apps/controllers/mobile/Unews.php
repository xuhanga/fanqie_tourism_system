<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 游记
 * @author chaituan@126.com
 */
class Unews extends WechatCommon {
	
	function index() {
		$this->load->model(array('admin/UserNews_model'));
		$data['items'] = $this->UserNews_model->getItems('','h_thumb,title,nickname,thumb,id','addtime desc');
		$this->load->view('mobile/unews/index',$data);
	}
	
	function upload(){
		if(is_ajax_request()){
			$data = Posts('data');
			$user = $this->User;
			$content = $_POST['content'];
			$data['content'] = implode('', $content);
			$this->load->model(array('admin/UserNews_model'));
			$data = array('uid'=>$user['id'],'nickname'=>$user['nickname'],'thumb'=>$user['thumb'],'h_thumb'=>$data['h_thumb'],'title'=>$data['title'],'content'=>$data['content'],'addtime'=>time());
			$id = $this->UserNews_model->add($data);
			is_AjaxResult($id,'添加成功');
		}else{
			$this->load->view('mobile/unews/upload');
		}
	}
	
	function detail(){
		$id = Gets('id');
		$this->load->model(array('admin/UserNews_model','admin/UserNewsCmt_model'));
		$data['item'] = $this->UserNews_model->getItem(array('id'=>$id));
		$this->UserNews_model->updates(array('hits'=>'+=1'),array('id'=>$id));
		$this->load->view('mobile/unews/detail',$data);
	}
}
