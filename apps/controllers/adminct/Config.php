<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 系统配置
 * @author chaituan@126.com
 */
class Config extends AdminCommon {

	public function __construct(){
		parent::__construct();
		$this->load->model('admin/Config_model','do');
	}
	
	public function index(){
		$data['items'] = $this->do->getItems('','','tkey desc');
		$this->load->view('admin/config/index',$data);
	}
	
	public function edit(){
		if(is_ajax_request()){
			$id = Posts('id','checkid');
			$data = Posts('data');
			$r = $this->do->updates($data,"id=$id");
			if($r){
				$this->do->cache();
				AjaxResult_ok();
			}else{
				AjaxResult_error();
			}
		}else{
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','checkid')));
			$this->load->view('admin/config/edit',$data);
		}
	}
	
	public function add(){
		if(is_ajax_request()){
			$data = Posts('data');
			$data['key'] = $data['tkey'].'_'.$data['key'];
			$r = $this->do->add($data);
			if($r){
				$this->do->cache();
				AjaxResult_ok();
			}else{
				AjaxResult_error();
			}
		}else{
			$this->load->view('admin/config/add');
		}
	}
	
	public function quicksave(){
		if(is_ajax_request()){
			$data = Posts('data');
			foreach ($data['hids'] as $k=>$id){
				if(!isset($data[$k]['val'])){//当checkbox不选中的时候 ，不传过来值 也不传过来name
					$v = 0;
				}else{
					$v = $data[$k]['val'];
				}
				$r = $this->do->updates(array('val'=>$v),"id=$id");
			}
			if($r){
				$this->do->cache();
				AjaxResult_ok();
			}else{
				AjaxResult_error();
			}
		}
	}
	
	
	public function del(){
		$r = $this->do->deletes("id=".Gets('id','checkid'));
		if($r){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
	
}
