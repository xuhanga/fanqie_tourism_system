<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 出发时间
 * @author chaituan@126.com
 */
class Tourismtime extends AdminCommon {
	
	function __construct() {
		parent::__construct ();
		$this->load->model ('admin/TourismTime_model','do');
	}
	
	function index() {
		$id = Gets('id','num');
		$where = $id?array('tid'=>$id):'';
		$data['items'] = $this->do->getItems($where);
		$data['tid'] = $id;
		$this->load->view('admin/tourismtime/index', $data);
	}
	
	function add(){
		if(is_ajax_request()){
			$data = Posts('data');
			$result = $this->do->add($data);
			is_AjaxResult($result);
		}else{
			$data['tid'] = Gets('tid','num');
			if(!$data['tid'])showmessage('错误','error');
			$this->load->view('admin/tourismtime/add', $data);
		}
	}
	
	function order(){
		$data['ttid'] = Gets('ttid');//时间
		$data['tid'] = Gets('tid');//活动
		$data['dr_url'] ='/adminct/tourismorder';
		$this->load->view('admin/tourismorder/index',$data);
	}
	
	function quicksave() {
		$data = Posts ();
		$this->do->quicksave($data);
	}
	
	
	function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
