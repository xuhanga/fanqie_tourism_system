<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 评价管理
 * @author chaituan@126.com
 */
class Comment extends AdminCommon {
	
	function __construct() {
		parent::__construct();
		$this->load->model('admin/Comment_model','do');
	}
	
	public function index() {
		$this->load->view('admin/order/comment');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"gtitle like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	
	
	//审核
	function check(){
		
	}
	
	public function del() {
		$result = $this->do->deletes("id=".Gets('id','checkid' ));
		is_AjaxResult($result);
	}
}
