<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 会员列表
 * @author chaituan@126.com
 */
class User extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/User_model'=>'do'));
	}
	
	public function config() {
		$data = get_Cache('admin_config');
		foreach ( $data as $v ) {
			if ($v ['tkey'] == 'user') {
				$datas ['items'][] = $v;
			}
		}
		$this->load->view ('admin/config/views', $datas);
	}
	
	public function index() {
		$data['fx'] = $this->do->getItems(array('fx'=>3,'gid'=>2),'id,nickname','id asc');
		$this->load->view ('admin/user/index',$data);
	}
	
	public function fx() {
		$this->load->view ('admin/user/fx');
	}
	
	public function leader() {
		$this->load->view ('admin/user/leader');
	}
	
	public function leader_iframe() {
		$this->load->view ('admin/user/leader_iframe');
	}
	
	function l_lists_iframe(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"state=1 and gid=2 and leader=3 and nickname like '%$name%'":'state=1 and gid=2 and leader=3';
		$data = $this->do->getItems ($where,'id,nickname,addtime,state,thumb,p_1','id',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		$data = get_Nickname($data);
		f_ajax_lists($total, $data);
	}
	
	function lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"nickname like '%$name%'":'';
		$data = $this->do->getItems($where,'id,nickname,addtime,state,thumb,p_1','id desc',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		$data = get_Nickname($data);
		f_ajax_lists($total, $data);
	}
	
	function fp(){
		if(is_ajax_request()){
			$ids = Posts('ids');
			$pid = Posts('pid');
			foreach ($ids as $v){
				$this->do->updates(array('p_1'=>$pid),array('id'=>$v,'p_1'=>null));
			}
			AjaxResult_ok('分配成功');
		}
	}
	
	function fx_lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"fx<>0 and nickname like '%$name%'":'fx<>0';
		$data = $this->do->getItems ($where,'id,nickname,addtime,state,thumb,gid,fx','fx',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		$data = get_Nickname($data);
		f_ajax_lists($total, $data);
	}
	
	function l_lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"leader<>0 and nickname like '%$name%'":'leader<>0';
		$data = $this->do->getItems ($where,'id,nickname,addtime,state,thumb,gid,leader','leader',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		$data = get_Nickname($data);
		f_ajax_lists($total, $data);
	}
	
	function leader_detail(){
		$uid = Gets('id');
		$this->load->model('admin/Leader_model');
		$data['item'] = $this->Leader_model->getItem(array('uid'=>$uid));
		$this->load->view ('admin/user/leader_detail',$data);
	}
	
	function sh_ok(){
		$id = Gets('id');
		$result = $this->do->updates(array('fx'=>3,'gid'=>2),"id=$id");
		is_AjaxResult($result);
	}
	
	function sh_no(){
		$id = Gets('id');
		$result = $this->do->updates(array('fx'=>2,'gid'=>2),"id=$id");
		is_AjaxResult($result);
	}
	
	function l_ok(){
		$id = Gets('id');
		$result = $this->do->updates(array('leader'=>3,'gid'=>2),"id=$id");
		is_AjaxResult($result);
	}
	
	function l_no(){
		$id = Gets('id');
		$result = $this->do->updates(array('leader'=>2,'gid'=>2),"id=$id");
		is_AjaxResult($result);
	}
	
	function leader_del(){
		$id = Gets('id');
		$user = $this->do->getItem(array('id'=>$id),'fx');
		if($user['fx']){
			$where = array('leader'=>0,'gid'=>2);
		}else{
			$where = array('leader'=>0,'gid'=>1);
		}
		$result = $this->do->updates($where,"id=$id");
		is_AjaxResult($result);
	}
	
	function fx_del(){
		$id = Gets('id');
		$user = $this->do->getItem(array('id'=>$id),'leader');
		if($user['leader']){
			$where = array('fx'=>0,'gid'=>2);
		}else{
			$where = array('fx'=>0,'gid'=>1);
		}
		$result = $this->do->updates($where,"id=$id");
		is_AjaxResult($result);
	}
	
	function del() {
		sleep(1);
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function lock(){
		sleep(1);
		$id = Gets('id','checkid');
		$open = Gets('open','checkid');
		$result = $this->do->updates(array('state'=>$open),array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function dels(){
		sleep(1);
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		is_AjaxResult($result);
	}
}
