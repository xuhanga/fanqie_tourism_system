<?php

defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 旅游订单
 * @author chaituan@126.com
 */
use EasyWeChat\Foundation\Application;
class Tourismorder extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/TourismOrder_model','do');
	}
	
	public function index() {
		$this->do->deletes("FROM_UNIXTIME(addtime,'%Y-%m-%d') < adddate(now(),-1) and state=1");
		$this->load->model ('admin/Tourism_model');
		$data['tourism'] = $this->Tourism_model->getItems('','id,title','id desc');
		$data['state'] = array('2'=>'已报名','3'=>'已结束','4'=>'已退出','5'=>'申请退出');
		$data['loaction'] = get_Cache('location');
		$this->load->view('admin/tourismorder/index',$data);
	}
	
	function lists(){
		$srk = Gets('srk');$tid = Gets('tid');$state = Gets('state');$stime = Gets('stime');$etime = Gets('etime');
		$where['state<>'] = "1";
		if($srk)$where['oid like'] = "%$srk%";
		if($tid)$where['tid'] = $tid;
		if($state)$where['state'] = $state;
		if($stime)$where['stime>='] = $stime;
		if($etime)$where['etime<='] = $etime;
		
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($srk&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function detail(){
		$id = Gets('id','num');
		$data['item'] = $this->do->getItem(array('id'=>$id));
		$this->load->view('admin/tourismorder/detail',$data);
	}
	
	function back(){
		if(is_ajax_request()){
			$data = Posts('data');
			$options = ['app_id'=>WX_APPID,'secret'=>WX_APPSecret,'payment' => ['merchant_id'=> WX_MCHID,'key'=> WX_KEY,'cert_path'=> SSLCERT_PATH,'key_path'=> SSLKEY_PATH,'notify_url'=> NOTIFY_URL]];
			$app = new Application($options);
			$result  = $app->payment->refundByTransactionId($data['wx_oid'],order_trade_no(),$data['total']*100);
			if($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
				$this->do->updates(array('state'=>4),array('id'=>$data['id']));
				AjaxResult_ok();
			}else{
				AjaxResult_error($result->return_msg.$result->err_code);
			}
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		$this->load->model('admin/Tourism_model');
		$v = $this->Tourism_model->getItem(array('aid'=>$id));
		if($v)AjaxResult_error("删除失败，已和活动关联");
		if($this->do->deletes("id=$id")){
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
	
	function export(){
		header("Content-type:application/vnd.ms-excel" );
		header('Content-Disposition: attachment;filename="' . date('YmdHis') . '.csv"');
		
		$srk = Gets('srk');$tid = Gets('tid');$state = Gets('state');$stime = Gets('stime');$etime = Gets('etime');
		$where['state<>'] = "1";
		if($srk)$where['oid like'] = "%$srk%";
		if($tid)$where['tid'] = $tid;
		if($state)$where['state'] = $state;
		if($stime)$where['stime>='] = $stime;
		if($etime)$where['etime<='] = $etime;
		
		$pre_count = 4000;
		$total_export_count = $this->do->count($where);
		$fp = fopen('php://output', 'a');
		$str = array('订单号','活动','开始时间','结束时间','价格','集合点','集合详情','商品','报名人员','状态');
		foreach($str as $key => $v) {
			$strs[] = iconv('utf-8','gb2312',$v);
		}
		fputcsv($fp, $strs);
		for ($i=1;$i<intval($total_export_count/$pre_count)+2;$i++){
			$result = $this->do->getItems($where,'*','',$i,$pre_count);
			foreach ($result as $k=>$v){
				$oid = iconv('utf-8','gb2312',$v['oid']); //中文转码
				$title = iconv('utf-8','gb2312',$v['title']);
				$stime = iconv('utf-8','gb2312',$v['stime']);
				$etime = iconv('utf-8','gb2312',$v['etime']?$v['etime']:$v['stime']);
				$total = iconv('utf-8','gb2312',$v['total']);
				$tvA = json_decode($v['tv'],true);
				$tv = iconv('utf-8','gb2312',$tvA['tvname']);
				$tvdetail = iconv('utf-8','gb2312',$tvA['tvname'].','.$tvA['tvusername'].','.$tvA['tvmobile'].','.$tvA['tvaddress'].','.$tvA['tvtime']);
				$tgs_goods = '';
				if($v['tgs_goods']){
					$tgs = json_decode($v['tgs_goods'],true);
					foreach ($tgs as $vs){
						$tgs_goods .= $vs['tgsname'].', x'.$vs['num'].','.$vs['tgsmoney'].'| ';
					}
				}
				$tgs_goods = iconv('utf-8','gb2312',$tgs_goods);
				
				$tgs_info = '';
				if($v['info']){
					$info = json_decode($v['info'],true);
					foreach ($info as $vss){
						$tgs_info .= $vss['username'].','.$vss['mobile'].','.$vss['code'].'| ';
					}
				}
				$tgs_info = iconv('utf-8','gb2312',$tgs_info);
				$state = '';
				if($v['state']==2){ 
					$state = "已报名";
				}elseif($v['state']==3){
					$state = "已结束";
				}elseif($v['state']==4){
					$state = "已退出";
				}elseif($v['state']==5){
					$state = "申请退出";
				}
				$state = iconv('utf-8','gb2312',$state);
				$new = array($oid,$title,$stime,$etime,$total,$tv,$tvdetail,$tgs_goods,$tgs_info,$state);
				fputcsv($fp, $new);
			}
			unset($new);
			ob_flush();
			flush();
		}
		exit;
	}
	
	
	function balance(){
		$items = $this->do->getItems("stime < now() and state=2",'id,uid,total,fx_money');
		if($items){
			foreach ($items as $v){
				$id[] = $v['id'];
				$fx[] = array('id'=>$v['uid'],'price'=>$v['total'],'fx_money'=>$v['fx_money']);
			}
			$id = implode(',', $id);
			$this->do->updates(array('state'=>3),"id in ($id)");
			$this->load->model(array('admin/User_model'));
			foreach ($fx as $v){
				$this->User_model->fx_fl($v['price'],$v['id'],$v['fx_money']);
			}
			AjaxResult_ok('成功处理'.count($items).'订单数据');
		}else{
			AjaxResult_error('没有要处理的数据');
		}
	}
	
}
