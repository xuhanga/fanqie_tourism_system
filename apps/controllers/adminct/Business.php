<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 商业合作
 * @author chaituan@126.com
 */
class Business extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Business_model'=>'do'));
	}
	

	public function index() {
		$this->load->view ('admin/business/index');
	}
	
	function lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"nickname like '%$name%'":'';
		$data = $this->do->getItems ($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}

	function del() {
		sleep(1);
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function dels(){
		sleep(1);
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		is_AjaxResult($result);
	}
}
