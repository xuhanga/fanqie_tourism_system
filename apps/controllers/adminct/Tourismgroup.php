<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 集合点
 * @author chaituan@126.com
 */
class Tourismgroup extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/TourismGroup_model','do');
	}
	
	public function index() {
		$this->load->view('admin/tourismgroup/index');
	}
	
	function lists(){
		$srk = Gets('srk');$num = Gets('num');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');$total = Gets('total','num');
		$where = '';
		if($srk)$where['title like'] = "%$srk%";
		$data = $this->do->getItems($where,'','sort,id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($srk&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/tourismgroup/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/tourismgroup/edit',$data);
		}
	}
	
	function edits(){
		if(is_ajax_request()){
			$id = Posts('id','num');
			$data = Posts('data');
			$result = $this->do->updates($data,array('id'=>$id));
			is_AjaxResult($result);
		}
	}
	
	function lock(){
		$id = Gets('id','num');
		$open = Gets('open','num');
		$result = $this->do->updates(array('state'=>$open),array('id'=>$id));
		is_AjaxResult($result);
	}

	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
