<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 商品分类
 *
 * @author chaituan@126.com
 */
class Goods_cat extends AdminCommon {
	function __construct() {
		parent::__construct ();
		$this->load->model ('admin/Goods_cat_model','do');
	}
	public function index() {
		$data['items'] = $this->do->getItems();
		$this->load->view('admin/goods_cat/index', $data);
	}
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts ( 'data' );
			if ($this->do->add ($data)) {
				$this->do->cache ();
				AjaxResult_ok ();
			} else {
				AjaxResult_error ();
			}
		} else {
			$this->load->view ('admin/goods_cat/add');
		}
	}
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache ();
				AjaxResult_ok ();
			} else {
				AjaxResult_error ();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id' => Gets('id','checkid')));
			$this->load->view('admin/goods_cat/edit',$data);
		}
	}
	
	public function del() {
		$r = $this->do->deletes ( "id=" . Gets ( 'id', 'checkid' ) );
		if ($r) {
			$this->do->cache ();
			AjaxResult_ok ();
		} else {
			AjaxResult_error ();
		}
	}
	
	function lock(){
		sleep(1);
		$id = Gets ('id','checkid');
		$open = Gets('open','checkid');
		$result = $this->do->updates(array('enabled'=>$open),array('id'=>$id));
		$this->do->cache ();
		is_AjaxResult($result);
	}
	
	function order_by() {
		$data = Posts ( 'data' );
		if (! $data) {
			AjaxResult ( '2', '不能为空' );
		}
		foreach ( $data as $k => $v ) {
			$datas [] = array (
					'id' => $k,
					'sort' => $v 
			);
		}
		$result = $this->do->update_batchs ( $datas, 'id' );
		if ($result) {
			$this->do->cache ();
			AjaxResult_ok ();
		} else {
			AjaxResult_error ();
		}
	}
}
