<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 广告管理
 * @author chaituan@126.com
 */
class Photo extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Photo_model'=>'do','admin/PhotoGroup_model'=>'do_group'));
	}
	
	public function index() {
		$this->load->view ('admin/photo/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"title like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function set(){
		$id = Gets('id');
		$item = $this->do->getItem(array('id'=>$id));
		$result = $this->do_group->updates(array('thumb'=>$item['thumb']),array('id'=>$item['gid']));
		is_AjaxResult($result);
	}

	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function dels() {
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		if ($result) {
			AjaxResult(1,"删除成功",$data['checked']);
		} else {
			AjaxResult(2,"删除失败");
		}
	}
}
