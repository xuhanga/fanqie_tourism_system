<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 用户文章评论管理
 * @author chaituan@126.com
 */
class Unews_cmt extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/UserNewsCmt_model'=>'do'));
	}
	
	public function index() {
		$data['id'] = Gets('id');
		$this->load->view ('admin/unewscmt/index',$data);
	}
	
	function lists(){
		$name = Gets('name');$id = Gets('id');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"unid=$id and says like '%$name%'":"unid=$id";
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function dels() {
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		if ($result) {
			AjaxResult(1,"删除成功",$data['checked']);
		} else {
			AjaxResult(2,"删除失败");
		}
	}
}
