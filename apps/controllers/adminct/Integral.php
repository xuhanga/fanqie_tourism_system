<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 积分
 * @author chaituan@126.com
 */
class Integral extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/Integral_model','do');
	}
	
	public function index() {
		$this->load->view('admin/user/integral');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"uid=$name or nickname like '%$name%'":'';
		$data = $this->do->getItems($where,'','uid desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function del() {
		$this->load->model ('admin/User_model');
		$id = Gets('id','checkid');
		$item = $this->do->getItem(array('id'=>$id));
		if($item['ands']=='-'){
			$data['integral'] = '+='.$item['num'];
		}else{
			$data['integral'] = '-='.$item['num'];
		}
		$this->User_model->updates($data,array('id'=>$item['uid']));
		if($this->do->deletes("id=$id")){
			
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
