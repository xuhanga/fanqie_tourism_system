<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 广告管理
 * @author chaituan@126.com
 */
class Ad extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Ad_model'=>'do','admin/AdGroup_model'=>'do_group'));
	}
	
	public function index() {
		$this->load->view ('admin/ad/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"ad.title like '%$name%'":'';
		$data = $this->do->getItems_join (array('ad_group' => "ad.catid=ad_group.id+left"),$where,'ad.*,ad_group.gname','ad.catid desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data['addtime'] = time();
			is_AjaxResult ( $this->do->add ( $data ) );
		} else {
			$data ['cat'] = $this->do->get_cache();
			$this->load->view ( 'admin/ad/add', $data );
		}
	}
	
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			is_AjaxResult($this->do->updates ( $data, "id=" . Posts ( 'id', 'checkid' ) ) );
		} else {
			$id = Gets ( "id", "checkid" );
			$data ['cat'] = $this->do->get_cache();
			$data ['item'] = $this->do->getItem("id=$id");
			$this->load->view ( 'admin/ad/edit', $data );
		}
	}
	
	function lock(){
		sleep(1);
		$id = Gets('id','checkid');
		$open = Gets('open','checkid');
		$result = $this->do->updates(array('state'=>$open),array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function dels() {
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		if ($result) {
			AjaxResult(1,"删除成功",$data['checked']);
		} else {
			AjaxResult(2,"删除失败");
		}
	}
}
