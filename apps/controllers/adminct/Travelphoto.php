<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Travelphoto extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/TravelPhoto_model'=>'do'));
	}
	
	function index() {
		$data['id'] = Gets('id');
		$this->load->view('admin/travelphoto/index',$data);
	}
	
	function lists(){
		$id = Gets('id');
		if(!$id)showmessage('错误','error');
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"tid=$id and nickname like '%$name%'":"tid=$id";
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	public function dels() {
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		if ($result) {
			AjaxResult(1,"删除成功",$data['checked']);
		} else {
			AjaxResult(2,"删除失败");
		}
	}
	
}
