<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 虚拟机人数管理
 * @author chaituan@126.com
 */
class Tourismvirtual extends AdminCommon {
	
	function __construct() {
		parent::__construct ();
		$this->load->model ('admin/TourismVirtual_model','do');
	}
	
	function index() {
		$id = Gets('id','num');
		$where = $id?array('tcid'=>$id):'';
		$data['items'] = $this->do->getItems($where);
		$data['tcid'] = $id;
		$this->load->view('admin/tourismvirtual/index', $data);
	}
	
	function add(){
		if(is_ajax_request()){
			$data = Posts('data');
			$this->load->model ('admin/TourismChild_model');
			$item = $this->TourismChild_model->getItem(array('id'=>$data['tcid']));
			$edit = array('sign_num'=>"+={$data['num']}");
			$total_num = $data['num'] + $item['sign_num'];
			if($total_num >= $item['line_num']){//到达指定的数量就执行
				$edit['state'] = 1;
			}
			$this->TourismChild_model->updates($edit,array('id'=>$data['tcid']));
			$result = $this->do->add($data);
			is_AjaxResult($result);
		}else{
			$data['tcid'] = Gets('tcid','num');
			if(!$data['tcid'])showmessage('错误','error');
			$this->load->view('admin/tourismvirtual/add', $data);
		}
	}
	
	function del() {
		$id = Gets('id','checkid');
		$item = $this->do->getItem(array('id'=>$id),'num,tcid');
		if($this->do->deletes("id=$id")){
			$this->load->model ('admin/TourismChild_model');
			$this->TourismChild_model->updates(array('sign_num'=>'-='.$item['num']),array('id'=>$item['tcid']));
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
