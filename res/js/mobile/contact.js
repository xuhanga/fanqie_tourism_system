Vue.component('mall-contact-card', {
	  template: `<div class="van-contact-card" :class="'van-contact-card--'+type" @click="onClick">
				    <div class="van-contact-card__content">
				      <template v-if="type === 'add'">
				        <van-icon class="van-contact-card__icon" name="add2" />
				        <div class="van-contact-card__text">添加联系人</div>
				      </template>
				      <template v-else-if="type === 'edit'" >
				      	<van-icon class="van-contact-card__icon" name="location" style="vertical-align: middle;"/>
				      	<div class="van-contact-card__text wd90">
				      		<div>{{name}} , {{tel}}</div>
				      		<div class="ellipsis wd100">{{address}}</div>
				      	</div>
				      	<van-icon class="van-contact-card__arrow" name="arrow" style="vertical-align: middle;"/>
				      </template>
				    </div>
				    <van-icon v-if="type === 'add'" class="van-contact-card__arrow" name="arrow" />
				  </div>`,
	props: {
	    type: {
	      type: String,
	      default: 'add'
	    },
	    tel: String,
	    name: String,
	    address: String,
	  },
	  methods: {
	    onClick(event) {
	    	this.$emit('click', event);
	    }
	  }
});

var username = '真实姓名';
var code = '身份证号';
var mobile = '电话号码';
Vue.component('van-contact-cards', {
	  template: `<div class="van-contact-card" :class="'van-contact-card--'+type" @click="onClick">
				    <div class="van-contact-card__content">
				      <template v-if="type === 'add'">
				        <van-icon class="van-contact-card__icon" name="add2" />
				        <div class="van-contact-card__text">添加联系人（可添加多个）</div>
				      </template>
				      <template v-else-if="type === 'edit'" >
				      	<van-cell-group>
					      <van-cell v-for="(item, index) in xz" :title="item.username+'，'+item.mobile"  :label="item.code" is-link ></van-cell>
				        </van-cell-group>
				      </template>
				    </div>
				    <van-icon v-if="type === 'add'" class="van-contact-card__arrow" name="arrow" />
				  </div>`,
	props: {
	    type: {
	      type: String,
	      default: 'add'
	    },
	    xz: {
		  type: Array,
	      default: () => []
	    }
	  },
	  methods: {
	    onClick(event) {
	    	this.$emit('click', event);
	    }
	  }
});

Vue.component('van-contact-lists', {
	  template: `<div class="van-contact-list">
	    <van-checkbox-group :value="value" @input="$emit('input', $event)">
	      <van-cell-group>
	        <van-cell v-for="(item, index) in list" :key="item.id" >
	          <van-checkbox :name="item" >
	            <p class="van-contact-list__text">{{ item.username }} ， {{ item.mobile }}</p>
	            <p class="van-contact-list__text  f12">{{code}}：{{ item.code }}</p>
	          </van-checkbox>
	          <van-icon slot="right-icon" name="edit" class="van-contact-list__edit" @click="$emit('edit', item, index)" />
	        </van-cell>
	      </van-cell-group>
	    </van-radio-group>
	    <van-row class="van-contact-list-bottom">
		  <van-col span="12">
		    <van-button bottom-action @click="$emit('close')">
			    <van-icon name="certificate" class="icon-bottom" ></van-icon>
			    <span class="icon-bottom"> 选中插入</span>
		    </van-button>
		  </van-col>
		  <van-col span="12">
		    <van-button type="primary" bottom-action @click="$emit('add')">
			    <van-icon name="add-o" class="icon-bottom" ></van-icon>
			    <span class="icon-bottom"> 添加联系人</span>
		    </van-button>
		  </van-col>
		</van-row>
	  </div>`,
	  props: {
	    value: {},
	    list: {
	      type: Array,
	      default: () => []
	    }
	  }
});

Vue.component('van-contact-edits', {
	  template: `<div class="van-contact-edit">
	    <van-cell-group>
	      <van-field v-model="data.username" maxlength="30" :label="username"   :placeholder="username"  :error="errorInfo.username"  @focus="onFocus('username')" />
	      <van-field v-model="data.mobile"  type="mobile" :label="mobile" :placeholder="mobile" :error="errorInfo.mobile"  @focus="onFocus('mobile')"    />
	      <van-field v-model="data.code"  type="codel" :label="code" :placeholder="code" :error="errorInfo.code"  @focus="onFocus('code')"    />
	    </van-cell-group>
	    <div class="van-contact-edit__buttons">
	      <van-button block :loading="isSaving" @click="onSaveContact" type="primary">保存</van-button>
	      <van-button block :loading="isDeleting" @click="onDeleteContact" v-if="isEdit">删除</van-button>
	      <van-button block  @click="$emit('close')" >关闭</van-button>
	    </div>
	  </div>`,
	  props: {
		  isEdit: Boolean,
		  isSaving: Boolean,
		  isDeleting: Boolean,
		  contactInfo: {
		      type: Object,
		      default: () => ({
		        id: '',
		        mobile: '',
		        username: '',codel:''
		      })
		  },
		  telValidator: {
	        type: Function,
	        default: function(value){
	        	if (!value) return false;
	        	value = value.replace(/[^-|\d]/g, '');
	        	return /^((\+86)|(86))?(1)\d{10}$/.test(value) || /^0[0-9\-]{10,13}$/.test(value);
	        }
	      },
	      codeValidator: {
		        type: Function,
		        default: function(value){
		        	if (!value) return false;
		        	if (value.length != 18) return false;
		            var wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
		            var vi = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
		            var ai = new Array(17);
		            var sum = 0;
		            var remaining, verifyNum;
		            for (var i = 0; i < 17; i++) ai[i] = parseInt(value.substring(i, i + 1));
		            for (var m = 0; m < ai.length; m++) sum = sum + wi[m] * ai[m];
		            remaining = sum % 11;
		            if (remaining == 2) verifyNum = "X";
		            else verifyNum = vi[remaining];
		            return (verifyNum == value.substring(17, 18));
		        }
		      }
	  },
	  data() {
	    return {
	      data: this.contactInfo,
	      errorInfo: {
	        username: false,
	        mobile: false,code:false
	      }
	    };
	  },
	  watch: {
		    contactInfo(val) {
		      this.data = val;
		    }
	  },
	  methods: {
	    onFocus(key) {
	      this.errorInfo[key] = false;
	    },
	    getErrorMessageByKey(key) {
	      const value = this.data[key];
	      switch (key) {
	        case 'username':
	          return value ? value.length <= 15 ? '' : '名字过长，请重新输入' : '请填写名字';
	        case 'mobile':
	          return this.telValidator(value) ? '' : '请填写正确的手机号码或电话号码';
	        case 'code':
		      return this.codeValidator(value) ? '' : '请填写正确的身份证号码';
	      }
	    },
	    onSaveContact() {
	      const items = ['username','mobile','code'];
	      const isValid = items.every(item => {
	        const msg = this.getErrorMessageByKey(item);
	        if (msg) {
	          this.errorInfo[item] = true;
	          this.$toast(msg);
	        }
	        return !msg;
	      });
	      if (isValid && !this.isSaving) {
	        this.$emit('save', this.data);
	      }
	    },
	    onDeleteContact() {
	      if (this.isDeleting) {
	        return;
	      }
	      this.$dialog.confirm({
	        message: '确定要删除这个联系人么'
	      }).then(() => {
	        this.$emit('delete', this.data);
	      },()=>{
	    	
	      });
	    }
	 }
});
